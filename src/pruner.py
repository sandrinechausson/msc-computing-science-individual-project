import torch
import spacy
import numpy as np


class Pruner():
    
    def __init__(self, description, one_hot_encodings, fix_len=1024):
        self.fix_len = fix_len
        self.get_concepts(description)
        keys = one_hot_encodings.args.keys()
        self.mask = {key: torch.full(one_hot_encodings.args[key].shape[:-1], 1) for key in keys}
        self.set_mask(description, one_hot_encodings.args)
    
    
    def get_concepts(self, description):
        nlp = spacy.load('en')
        self.concepts = list({x for tok in nlp.tokenizer(description) for x in (tok.text, tok.text + " ")})
        # self.concpt_letters = {tuple(c) for c in self.concepts}

        
    def set_mask(self, description, one_hot):
        
        keys = list(one_hot.keys())
        
        tokenized = list(description)
        if len(tokenized) < self.fix_len:
            tokenized += ['<PAD>'] * (self.fix_len - len(tokenized))

        for key in keys:
            tokenized_array = np.full(one_hot[key].shape, tokenized, dtype=np.dtype(str))
            mask_1 = one_hot[key].detach().numpy().astype(np.dtype(bool))
            tokenized_array[mask_1 == False] = ''
            words = [[[''.join(x) for x in tokenized_array[0][k]] for k in range(tokenized_array.shape[1])]]
            words = np.array(words)
            mask = np.isin(words, self.concepts)
            mask = torch.from_numpy(mask)
            self.mask[key] = mask * self.mask[key]
            
    
    def set_explainer(self, explainer):
        self.explainer = explainer
        keys = list(self.mask.keys())
        comp_keys = list(self.explainer.comp_rel.keys())
        for key in keys:
            if key != "Embedded":
                self.explainer.net_rel[key] = self.mask[key] * explainer.net_rel[key]
                self.explainer.net_filters_collapsed[key] = self.mask[key] * explainer.net_filters_collapsed[key]
                for comp_key in comp_keys:
                    self.explainer.comp_rel[comp_key][key] = self.mask[key] * explainer.comp_rel[comp_key][key]
                    self.explainer.comp_filters_collapsed[comp_key][key] = self.mask[key] * explainer.comp_filters_collapsed[comp_key][key]
                    
                    
                    