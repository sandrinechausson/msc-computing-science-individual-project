import torch




def one_hot_conv(INPUT, OUT_SIZE, KS, STR = 1, PAD = None):
      
    INPUT = INPUT.any(dim=1, keepdim=True)
    conved = []
    seq_len = INPUT.shape[2]
        
    for i in range(0, seq_len, STR):
            
        if PAD != None:
            START = max(i-PAD, 0)
            END = min(seq_len, i - PAD + KS)
        else:
            START = i
            if START > (seq_len - KS):
                break
            END = min(seq_len, START + KS)
        c = INPUT[0][0][START:END].any(dim=0, keepdim=True)
        conved.append(c)
        
    result = torch.cat(conved, dim=0).unsqueeze(0).unsqueeze(0)
    result[result!=0] = 1
    result = result.repeat(1, OUT_SIZE, 1, 1)

    return result



def maxpool(INPUT, INDICES, STRIDE = 1):
    
    INDICES = INDICES.unsqueeze(-1)
    INDICES = INDICES.expand(INDICES.shape[0], INDICES.shape[1], INDICES.shape[2], INPUT.shape[3])
    result = torch.gather(INPUT, 2, INDICES)
        
    return result



class OneHot_ShallowCNN():
    
    def __init__(self, input_size):
        
        self.args = {}
        self.args["Words"] = torch.eye(input_size, dtype=torch.bool).unsqueeze(0).unsqueeze(0)
        self.args["2-grams"] = one_hot_conv(self.args["Words"], 50, 2)
        self.args["3-grams"] = one_hot_conv(self.args["Words"], 50, 3)
        self.args["4-grams"] = one_hot_conv(self.args["Words"], 50, 4)



class OneHot_DeeperCNN():
    
    def __init__(self, input_size, indices):
        
        self.args = {}
        self.args["Words"] = torch.eye(input_size, dtype=torch.bool).unsqueeze(0).unsqueeze(0)
        maxpool_A = []
        for i in range(3):
            self.args[str(i+2) + "-grams"] = one_hot_conv(self.args["Words"], 50, i+2)
            maxpooled_A = maxpool(self.args[str(i+2) + "-grams"], indices[i].unsqueeze(-1)).squeeze(2)
            maxpool_A.append(maxpooled_A)
            
        cat_data = torch.cat(maxpool_A, dim=1).unsqueeze(0)
        self.args["Conved 2"] = one_hot_conv(cat_data, 50, 2)
    


class OneHot_VDCNN():
    
    def __init__(self, *args, **kwargs):
        
        self.args = {}
        self.args["Embedded"] = torch.eye(1024, dtype=torch.bool).unsqueeze(0).unsqueeze(0)
        self.args["1st conv"] = one_hot_conv(self.args["Embedded"], 64, kwargs['kernel_size'], kwargs['stride'], kwargs['padding'])
        next_A = self.args["1st conv"]
        self.counter = 1
        
        for para in args:
            dic, next_A = self.res_block(next_A, kwargs['kernel_size'], kwargs['stride'], kwargs['padding'], *para)
            self.args.update(dic)
            self.counter += 1
        
        
    def res_block(self, INPUT, kernel_size, stride, padding, OUT_SIZE, INDICES = None):
        
        args = {}
        first_stride = 1
        pool_stride = 2 if (INDICES != None) else 1

        # architecture
        dic, next_A = self.conv_block(INPUT, OUT_SIZE, kernel_size, stride, padding, first_stride)
        args.update(dic)
        if INDICES != None: # VGG-like
            next_A = maxpool(next_A, INDICES, pool_stride)
        args["Res block " + str(self.counter) + " shortcut"] = one_hot_conv(INPUT, OUT_SIZE, 1, pool_stride)
        
        result = next_A + args["Res block " + str(self.counter) + " shortcut"]
        result[result!=0] = 1
        args["Res block " + str(self.counter)] = result
        
        return args, result
        
        
    def conv_block(self, INPUT, OUT_SIZE, kernel_size, stride, padding, first_stride):

        args = {}
        args["Res block " + str(self.counter) + " conv 1"] = one_hot_conv(INPUT, OUT_SIZE, kernel_size, first_stride, padding)
        args["Res block " + str(self.counter) + " conv 2"] = one_hot_conv(args["Res block " + str(self.counter) + " conv 1"], OUT_SIZE, kernel_size, stride, padding)

        return args, args["Res block " + str(self.counter) + " conv 2"]
