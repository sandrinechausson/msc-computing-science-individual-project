import random
import os.path

import torch
from torchtext import datasets
from torchtext.data import Field, LabelField, TabularDataset, BucketIterator


def preprocess(data_name, model_type, path, rand_seed, max_vocab_size, batch_size, device):
    
    if model_type in ('ShallowCNN', 'DeeperCNN', 'RNN'):
        tokenizer = 'spacy'
        fix_len = None
        vectors = "glove.6B.100d"
        unk_init = torch.Tensor.normal_
        
    elif model_type in ('VDCNN_17', 'VDCNN_29'):
        tokenizer = list
        fix_len = 1024
        vectors = None
        unk_init = None
        
    else:
        print(model_type, " is not a valid model type. Must be one of 'ShallowCNN', 'DeeperCNN', 'RNN', 'VDCNN_17' or 'VDCNN_29'.")
    
    TEXT = Field(tokenize = tokenizer, fix_length = fix_len, batch_first = True)
    
    if data_name in ('News_Title', 'News_Description'):
        LABEL = LabelField(dtype = torch.long)
        if data_name == 'News_Title':
            datafields = [("Index", LABEL),("Title", TEXT),("Description", None)]
        else:
            datafields = [("Index", LABEL),("Title", None),("Description", TEXT)]
                
        train_data, test_data = TabularDataset.splits(path=os.path.join(path, "news_data"), 
                                                      train='train.csv', 
                                                      test="test.csv", format='csv',
                                                      skip_header=True,
                                                      fields=datafields)
    
    elif data_name == 'Sentiment':
        # Load data from torchtext
        LABEL = LabelField(dtype = torch.float)
        train_data, test_data = datasets.IMDB.splits(TEXT, LABEL)
    
    else: 
        print(data_name, " is not a valid data name. Must be one of 'Sentiment', 'News_Title' or 'News_Description'.")
        
    random.seed(rand_seed)
    train_data, valid_data = train_data.split(random_state=random.getstate())

    TEXT.build_vocab(train_data, max_size = max_vocab_size, 
                     vectors = vectors, 
                     unk_init = unk_init)
        
    LABEL.build_vocab(train_data)

    train_iterator, valid_iterator, test_iterator = BucketIterator.splits(
            (train_data, valid_data, test_data), batch_size = batch_size, 
            device = device, sort=False)
    
    return TEXT, LABEL, train_iterator, valid_iterator, test_iterator


