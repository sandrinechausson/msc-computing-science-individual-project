import torch



def normalise(dic, g_max):
    
    keys = list(dic.keys())
    result = {}
    
    for k in keys:
        result[k] = dic[k]/g_max
    
    return result


def get_max_and_min(dic):
        
    #  Find max and min
    max_val = 0
    min_val = 0
    
    keys = dic.keys()
    
    for k in keys:
                    
        this_max = torch.max(dic[k]).item()
        this_min = torch.min(dic[k]).item()
            
        if this_max > max_val:
            max_val = this_max
                
        if this_min < min_val:
            min_val = this_min
    
    return max_val, min_val