import time
import os

import torch

from .visuals import show_lossaccevol


# Helper functions
def accuracy_binary(preds, y):
    """ Return accuracy per batch. """
    correct = (torch.round(torch.sigmoid(preds)) == y).float()
    return correct.sum() / len(correct)


# Helper functions
def accuracy_multi(preds, y):
    """ Return accuracy per batch. """
    _, predicted = torch.max(preds.data, 1)
    correct = (predicted == y).float() 
    return correct.sum() / len(correct)


def epoch_time(start_time, end_time):
    '''Track training time. '''
    elapsed_time = end_time - start_time
    elapsed_mins = int(elapsed_time / 60)
    elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
    return elapsed_mins, elapsed_secs



def train_CNN(model, iterator, optimizer, criterion, data_name):
    '''Train the model with specified data, optimizer, and loss function. '''
    epoch_loss = 0
    epoch_acc = 0
    
    model.train()
    
    for batch in iterator:
        
        # Reset the gradient to not use them in multiple passes 
        optimizer.zero_grad()
        
        if data_name == 'Sentiment':
            output, _ = model(batch.text)
            predictions = output.squeeze(1)
            loss = criterion(predictions, batch.label)
            acc = accuracy_binary(predictions, batch.label)
        elif data_name == 'News_Title':
            output, _ = model(batch.Title)
            predictions = output.squeeze(1)
            loss = criterion(predictions, batch.Index)
            acc = accuracy_multi(predictions, batch.Index)
        elif data_name == 'News_Description':
            output, _ = model(batch.Description)
            predictions = output.squeeze(1)
            loss = criterion(predictions, batch.Index)
            acc = accuracy_multi(predictions, batch.Index)
        else:
            print(data_name, " is not a valid data name. Must be one of 'Sentiment', 'News_Title' or 'News_Description'.")

        
        # Backprop
        loss.backward()
        
        # Optimize the weights
        optimizer.step()
        
        # Record accuracy and loss
        epoch_loss += loss.item()
        epoch_acc += acc.item()
        
    return epoch_loss / len(iterator), epoch_acc / len(iterator)

    
def evaluate_CNN(model, iterator, criterion, data_name):
    '''Evaluate model performance. '''
    epoch_loss = 0
    epoch_acc = 0
    
    # Turm off dropout while evaluating
    model.eval()
    
    # No need to backprop in eval
    with torch.no_grad():
    
        for batch in iterator:

            if data_name == 'Sentiment':
                output, _ = model(batch.text)
                predictions = output.squeeze(1)
                loss = criterion(predictions, batch.label)
                acc = accuracy_binary(predictions, batch.label)
            elif data_name == 'News_Title':
                output, _ = model(batch.Title)
                predictions = output.squeeze(1)
                loss = criterion(predictions, batch.Index)
                acc = accuracy_multi(predictions, batch.Index)
            elif data_name == 'News_Description':
                output, _ = model(batch.Description)
                predictions = output.squeeze(1)
                loss = criterion(predictions, batch.Index)
                acc = accuracy_multi(predictions, batch.Index)
            

            epoch_loss += loss.item()
            epoch_acc += acc.item()
        
    return epoch_loss / len(iterator), epoch_acc / len(iterator) 



def train_RNN(model, iterator, optimizer, criterion, data_name):
    '''Train the model with specified data, optimizer, and loss function. '''
    epoch_loss = 0
    epoch_acc = 0
    
    model.train()
    
    for batch in iterator:
        
        # Reset the gradient to not use them in multiple passes 
        optimizer.zero_grad()
        hidden = model.initHidden(len(batch))
        if data_name == 'Sentiment':
            output, _, _ = model(batch.text, hidden)
            loss = criterion(output.squeeze(0).squeeze(-1), batch.label)
            acc = accuracy_multi(output.squeeze(0), batch.label)
        elif data_name == 'News_Title':
            output, _, _ = model(batch.Title, hidden)
            loss = criterion(output.squeeze(0), batch.Index)
            acc = accuracy_multi(output.squeeze(0), batch.Index)
        elif data_name == 'News_Description':
            output, _, _ = model(batch.Description, hidden)
            loss = criterion(output.squeeze(0), batch.Index)
            acc = accuracy_multi(output.squeeze(0), batch.Index)
        
        # Backprop
        loss.backward()
        
        # Optimize the weights
        optimizer.step()
        
        # Record accuracy and loss
        epoch_loss += loss.item()
        epoch_acc += acc.item()
        
    return epoch_loss / len(iterator), epoch_acc / len(iterator)


def evaluate_RNN(model, iterator, criterion, data_name):
    '''Evaluate model performance. '''
    epoch_loss = 0
    epoch_acc = 0
    
    # Turm off dropout while evaluating
    model.eval()
    
    # No need to backprop in eval
    with torch.no_grad():
    
        for batch in iterator:
            
            hidden = model.initHidden(len(batch))
            if data_name == 'Sentiment':
                output, _, _ = model(batch.text, hidden)
                loss = criterion(output.squeeze(0).squeeze(-1), batch.label)
                acc = accuracy_multi(output.squeeze(0), batch.label)
            if data_name == 'News_Title':
                output, _, _ = model(batch.Title, hidden)
                loss = criterion(output.squeeze(0), batch.Index)
                acc = accuracy_multi(output.squeeze(0), batch.Index)
            if data_name == 'News_Description':
                output, _, _ = model(batch.Description, hidden)
                loss = criterion(output.squeeze(0), batch.Index)
                acc = accuracy_multi(output.squeeze(0), batch.Index)                

            epoch_loss += loss.item()
            epoch_acc += acc.item()
        
    return epoch_loss / len(iterator), epoch_acc / len(iterator) 



def train_and_evaluate(model, optimizer, criterion, data_name, model_type, 
                       train_iterator, valid_iterator, test_iterator, path, 
                       epochs = None):
    
    if epochs == None:
        epochs = 10
    
    best_valid_loss = float('inf')
    val_loss = []
    val_acc = []
    tr_loss = []
    tr_acc = []

    for epoch in range(epochs):
    
        # Calculate training time
        start_time = time.time()
    
        # Get epoch losses and accuracies 
        if model_type in ('ShallowCNN', 'DeeperCNN', 'VDCNN_17', 'VDCNN_29'):
            train_loss, train_acc = train_CNN(model, train_iterator, optimizer, criterion, data_name)
            valid_loss, valid_acc = evaluate_CNN(model, valid_iterator, criterion, data_name)
        elif model_type == 'RNN':
            train_loss, train_acc = train_RNN(model, train_iterator, optimizer, criterion, data_name)
            valid_loss, valid_acc = evaluate_RNN(model, valid_iterator, criterion, data_name)        
        else:
            print(model_type, " is not a valid model type. Must be one of 'ShallowCNN', 'DeeperCNN', 'RNN', 'VDCNN_17' or 'VDCNN_29'.")
    
        end_time = time.time()
        epoch_mins, epoch_secs = epoch_time(start_time, end_time)
    
        # Save training metrics
        val_loss.append(valid_loss)
        val_acc.append(valid_acc)
        tr_loss.append(train_loss)
        tr_acc.append(train_acc)
    
        if valid_loss < best_valid_loss:
            best_valid_loss = valid_loss
            torch.save(model.state_dict(), os.path.join(path,"model.pt"))
    
        print(f'Epoch: {epoch+1:2} | Epoch Time: {epoch_mins}m {epoch_secs}s')
        print(f'\tTrain Loss: {train_loss:.3f} | Train Acc: {train_acc*100:.2f}%')
        print(f'\t Val. Loss: {valid_loss:.3f} |  Val. Acc: {valid_acc*100:.2f}%')
    

    # Plot accuracy and loss
    show_lossaccevol(tr_loss, val_loss, tr_acc, val_acc, path)

    # Evaluate model on test data
    model.load_state_dict(torch.load(os.path.join(path,"model.pt")))
    
    if model_type in ('ShallowCNN', 'DeeperCNN', 'VDCNN_17', 'VDCNN_29'):
        test_loss, test_acc = evaluate_CNN(model, test_iterator, criterion, data_name)
    elif model_type == 'RNN':
        test_loss, test_acc = evaluate_RNN(model, test_iterator, criterion, data_name)

    print(f'Test Loss: {test_loss:.3f} | Test Acc: {test_acc*100:.2f}%')
    
    