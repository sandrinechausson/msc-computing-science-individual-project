import os

import numpy as np
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker



CDICT = {'red':  ((0.0, 0.8, 0.8),   # no red at 0
                  (0.5, 1.0, 1.0),   # all channels set to 1.0 at 0.5 to create white
                  (1.0, 0.0, 0.0)),  # set to 0.8 so its not too bright at 1

         'green': ((0.0, 0.0, 0.0),   # set to 0.8 so its not too bright at 0
                   (0.5, 1.0, 1.0),   # all channels set to 1.0 at 0.5 to create white
                   (1.0, 0.8, 0.8)),  # no green at 1

         'blue':  ((0.0, 0.0, 0.0),   # no blue at 0
                   (0.5, 1.0, 1.0),   # all channels set to 1.0 at 0.5 to create white
                   (1.0, 0.0, 0.0))   # no blue at 1
         }


REV_CDICT = {'red':  ((0.0, 0.0, 0.0),   # no red at 0
                      (0.5, 1.0, 1.0),   # all channels set to 1.0 at 0.5 to create white
                      (1.0, 0.8, 0.8)),  # set to 0.8 so its not too bright at 1

             'green': ((0.0, 0.8, 0.8),   # set to 0.8 so its not too bright at 0
                       (0.5, 1.0, 1.0),   # all channels set to 1.0 at 0.5 to create white
                       (1.0, 0.0, 0.0)),  # no green at 1

             'blue':  ((0.0, 0.0, 0.0),   # no blue at 0
                       (0.5, 1.0, 1.0),   # all channels set to 1.0 at 0.5 to create white
                       (1.0, 0.0, 0.0))   # no blue at 1
            }


def show_explanation(STRENGTH, Y_LABELS, X_LABELS, TITLE, g_max = 1, g_min = -1, reverse = False):
    
    cdict = None
    
    if reverse:
        # This dictionary defines the colormap
        cdict = REV_CDICT
    
    else:
        # This dictionary defines the colormap
        cdict = CDICT
        
        
    # Create the colormap using the dictionary
    GnRd = colors.LinearSegmentedColormap('GnRd', cdict)

    fig, ax = plt.subplots()
    fig.set_figheight(2)
    fig.set_figwidth(12)
    
    # Plot data
    p = ax.pcolormesh(np.flipud(STRENGTH),cmap=GnRd,vmin=g_min,vmax=g_max)

    # Labels
    ax.set_xticks(np.arange(0.5, len(STRENGTH[0])))
    ax.set_xticklabels(X_LABELS)
    ax.set_yticks(np.arange(0.5, len(Y_LABELS)))
    ax.set_yticklabels(Y_LABELS)
    
    # Make a colorbar
    fig.colorbar(p,ax=ax)
    ax.set_title(TITLE)

    plt.show()
 
    
 
def show_lossaccevol(tr_loss, val_loss, tr_acc, val_acc, path=None):
    # Plot accuracy and loss
    fig, ax = plt.subplots(1, 2, figsize=(15,5))
    ax[0].plot(val_loss, label='Validation loss')
    ax[0].plot(tr_loss, label='Training loss')
    ax[0].set_title('Losses')
    ax[0].set_xlabel('Epoch')
    ax[0].set_ylabel('Loss')
    ax[0].legend()
    ax[1].plot(val_acc, label='Validation accuracy')
    ax[1].plot(tr_acc, label='Training accuracy')
    ax[1].set_title('Accuracies')
    ax[1].set_xlabel('Epoch')
    ax[1].set_ylabel('Accuracy')
    plt.legend()
    
    if path == None:
        plt.show()
    else: 
        plt.savefig(os.path.join(path,"lossaccevol.png"))
    
    
    
def annotated_heatmap(STRENGTH, ANNOTATION, TITLE, g_max = 2, g_min = -2, reverse = False):
    
    cdict = None
    
    if reverse:
        # This dictionary defines the colormap
        cdict = REV_CDICT
    
    else:
        # This dictionary defines the colormap
        cdict = CDICT
        
        
    # Create the colormap using the dictionary
    GnRd = colors.LinearSegmentedColormap('GnRd', cdict)

    fig, ax = plt.subplots()
    fig.set_figheight(1)
    fig.set_figwidth(12)
    
    # Plot data
    p = ax.pcolormesh(np.flipud(STRENGTH),cmap=GnRd,vmin=g_min,vmax=g_max)

    # Labels
    ax.set_xticks(np.arange(len(STRENGTH[0])))
    ax.set_yticks(np.arange(len(STRENGTH)))


    for i in range(len(STRENGTH)):
        for j in range(len(STRENGTH[0])):
            text = ax.text(j+0.5, i+0.5, ANNOTATION[i][j], ha="center", va="center", color="black")
    
    # Make a colorbar
    fig.colorbar(p,ax=ax)
    ax.set_title(TITLE)
    plt.axis('off')
    plt.show()
  
    
def confusion_matrix(confusion, all_categories):
    # Set up plot
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(confusion.numpy())
    fig.colorbar(cax)

    # Set up axes
    ax.set_xticklabels([''] + all_categories, rotation=90)
    ax.set_yticklabels([''] + all_categories)

    # Force label at every tick
    ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))

    plt.show()



def heatmap(STRENGTH, TITLE, ANNOTATION = None, Y_LABELS = None, X_LABELS = None, g_max = 2, g_min = -2, reverse = False):
    
    if reverse:
        # This dictionary defines the colormap
        cdict = REV_CDICT
    
    else:
        # This dictionary defines the colormap
        cdict = CDICT
        
        
    # Create the colormap using the dictionary
    GnRd = colors.LinearSegmentedColormap('GnRd', cdict)

    fig, ax = plt.subplots()
    fig.set_figheight(1)
    fig.set_figwidth(12)
    
    # Plot data
    p = ax.pcolormesh(np.flipud(STRENGTH),cmap=GnRd,vmin=g_min,vmax=g_max)

    # Labels
    plt.axis('off')
    
    ax.set_xticks(np.arange(len(STRENGTH[0])))
    if X_LABELS:
        plt.axis('on')
        ax.set_xticks(0.5, np.arange(len(STRENGTH[0])))
        ax.set_xticklabels(X_LABELS)
        
    ax.set_yticks(np.arange(len(STRENGTH)))
    if Y_LABELS:
        plt.axis('on')
        ax.set_yticks(0.5, np.arange(len(STRENGTH)))
        ax.set_yticklabels(Y_LABELS)
    
    if ANNOTATION:
        for j in range(len(STRENGTH[0])):
            text = ax.text(j+0.5, 0.5, ANNOTATION[j], ha="center", va="center", color="black")
            plt.axis('off')
    
    # Make a colorbar
    fig.colorbar(p,ax=ax)
    ax.set_title(TITLE)
    plt.show()
    
    
    
def colorize(color_array, words):
    ''' 
    Function that created HTML file with word strength visualisation
    Source: https://gist.github.com/ihsgnef/f13c35cd46624c8f458a4d23589ac768
    ''' 
    # words is a list of words
    # color_array is an array of numbers between 0 and 1 of length equal to words
    cmap = colors.LinearSegmentedColormap('GnRd', CDICT)
    template = '<span class="clickable"><span id={} class="barcode"; style="display: inline-block; color: black; background-color: {}">{}</span></span>'
    colored_string = ''
    counter = 0
    for word, color in zip(words, color_array):
        color = colors.rgb2hex(cmap((color+1)/2)[:3])
        colored_string += template.format(counter, color, '&nbsp' + word + '&nbsp')
        counter += 1
    return colored_string



def colorize_filter(prefix, filter_nb, strength, concepts):
    
    cmap = colors.LinearSegmentedColormap('GnRd', CDICT)
    template = '''<div style="text-align:center;">
    <p><b>{}-gram filter {}</b></p>
	<div style="background-color: {}">
		<p style="color: black; text-align: center">{}</p>
    	<p style="color: black; text-align: center">{}</p>
    	<p style="color: black; text-align: center">{}</p>
    </div>
    </div>'''
    color = colors.rgb2hex(cmap((strength+1)/2)[:3])
    colored_string = template.format(prefix, str(filter_nb), color, concepts[0], concepts[1], concepts[2])
    return colored_string

