import torch
import copy


def regular(a, layer, next_R, eps = 0):
    ''' Efficient implementation of LRP-0 from https://link.springer.com/chapter/10.1007/978-3-030-28954-6_10'''
    a = (a.data).requires_grad_(True)
    z = layer.forward(a)
    if type(z) is tuple:
        z = z[0]
    s = next_R / (z + eps + 1e-9)
    (z*s.data).sum().backward()
    c = a.grad
    R = a*c 
    return R



def alpha_beta(a, layer, next_R, alpha = 2, beta = 1): # Best so far...
    '''
    LRP alpha beta according to Eq(60) in DOI: 10.1371/journal.pone.0130140
    '''   
    pos_layer = copy.deepcopy(layer)
    neg_layer = copy.deepcopy(layer)
    try:
        weights = layer.weight.data
        pos_layer.weight.data = torch.clamp(weights, min=0.0)
        pos_layer.bias.data.fill_(0.0)
        neg_layer.weight.data = torch.clamp(weights, max=0.0)
        neg_layer.bias.data.fill_(0.0)
    except AttributeError:
        print("This is maxpool")
        pass
    
    a_1 = (a.data).requires_grad_(True)
    pos_a = torch.clamp(a_1, min=0.0)
    neg_a = torch.clamp(a_1, max=0.0)
    pos_z = pos_layer(pos_a) + neg_layer(neg_a)
    if type(pos_z) is tuple:
        pos_z = pos_z[0]
        
    try:
        pos_biases = torch.clamp(layer.bias.data, min=0.0)
        pos_z = pos_z + pos_biases.unsqueeze(0).unsqueeze(-1)
    except AttributeError:
        pass
    
    pos_s = next_R / (pos_z + 1e-9)
    (pos_z * pos_s.data).sum().backward()
    pos_c = a_1.grad
    pos_R = a_1 * pos_c
    
    
    a_2 = (a.data).requires_grad_(True)
    pos_a = torch.clamp(a_2, min=0.0)
    neg_a = torch.clamp(a_2, max=0.0)
    neg_z = pos_layer(neg_a) + neg_layer(pos_a)
    if type(neg_z) is tuple:
        neg_z = neg_z[0]
        
    try:
        neg_biases = torch.clamp(layer.bias.data, max=0.0)
        neg_z = neg_z + neg_biases.unsqueeze(0).unsqueeze(-1)
    except AttributeError:
        pass
    
    neg_s = next_R / (neg_z - 1e-9)
    (neg_z * neg_s.data).sum().backward()
    neg_c = a_2.grad
    neg_R = a_2 * neg_c
    
    R = alpha * pos_R - beta * neg_R
    
    return R
    



def maxpool(a, indices, next_R):
    ''' LRP-0 rule for maxpooling used in 
    shallow CNN where efficiency is not an issue'''
    R = torch.full(a.shape, 0, dtype=torch.float)
    for i in range(indices.shape[1]):
        R[0][i][indices[0][i]] += next_R[0][i]
    return R



def maxpool_2(a, indices, next_R):
    ''' Inefficient impelementation of LRP-0 rule for maxpooling used in 
    shallow CNN where efficiency is not an issue'''    
    R = torch.full(a.shape, 0, dtype=torch.float)
    n_filters = indices.shape[1]
    n_pooled = indices.shape[2]
    for i in range(n_filters):
        for j in range(n_pooled):
            R[0][i][indices[0][i][j]] += next_R[0][i][j]
    
    return R  



def bias(bias, z, next_R, eps = 0):
    ''' Implementation of LRP-0 for biases'''
    s = next_R / (z + eps + 1e-9)
    bias = bias.unsqueeze(0)
    while s.shape != bias.shape: 
        bias = bias.unsqueeze(len(bias.shape))
        bias = bias.expand(s.shape)
    R = bias * s
    return R


def regular_for_rnn(a, layer, next_R, eps = 0):
    ''' Regular implementation of LRP-0 which also returns s '''
    a = (a.data).requires_grad_(True)
    z = layer.forward(a)
    if type(z) is tuple:
        z = z[0]
    s = next_R / (z + eps + 1e-9)
    (z*s.data).sum().backward()
    c = a.grad
    R = a*c 
    return R, s
