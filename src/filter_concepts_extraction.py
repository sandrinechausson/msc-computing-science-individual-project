import pandas as pd
import torch
import spacy
import textacy
import pickle

from spacy.tokens import Doc 



def top_k_sentences(model, data_name, FILE_PATH, DATA_PATH, tokenizer, device, vocab, K = 10, min_len = 5):
    '''Retrieve top-K activated sentences per filter '''
    
    # Turm off dropout while evaluating
    model.eval()
        
    topK = {}
    topK["2-grams"] = [{} for f in range(50)]
    topK["3-grams"] = [{} for f in range(50)]
    topK["4-grams"] = [{} for f in range(50)] 
    
    df = pd.read_csv(DATA_PATH + '/train.csv', index_col=0)
    counter = 0
    
    # No need to backprop in eval
    with torch.no_grad():
        
        if data_name == 'News_Title':
            for title in df["Title"]:
            
                counter += 1
                tokenized = [tok.text for tok in tokenizer(title)]
                if len(tokenized) < min_len:
                    tokenized += ['<pad>'] * (min_len - len(tokenized))
            
                # Map words to word embeddings
                indexed = [vocab.stoi[t] for t in tokenized]
                tensor = torch.LongTensor(indexed).to(device)
                tensor = torch.LongTensor(indexed)
                tensor = tensor.unsqueeze(0)
                _, mid = model(tensor)
            
                avg = {}
                avg['2-grams'] = torch.mean(mid["Conved"][0], dim=-1)
                avg['3-grams'] = torch.mean(mid["Conved"][1], dim=-1)
                avg['4-grams'] = torch.mean(mid["Conved"][2], dim=-1)
            
                for i in range(50):
                
                    for j in range(3):
                        val = avg[str(j+2) + '-grams'][0][i]
                        if len(topK[str(j+2) + "-grams"][i]) < K:
                            topK[str(j+2) + "-grams"][i][val] = title
                        else:
                            min_i = min(topK[str(j+2) + "-grams"][i].keys())
                            if val > min_i:
                                del topK[str(j+2) + "-grams"][i][min_i]
                                topK[str(j+2) + "-grams"][i][val] = title
        
        
        elif data_name == 'News_Description':
            
            nlp = spacy.load('en_core_web_lg')
            for descr in df["Description"]:
            
                sentences = [i.text for i in nlp(descr).sents]
            
                for sentence in sentences:
            
                    counter += 1
                    tokenized = [tok.text for tok in tokenizer(sentence)]
                    if len(tokenized) < min_len:
                        tokenized += ['<pad>'] * (min_len - len(tokenized))
            
                    # Map words to word embeddings
                    indexed = [vocab.stoi[t] for t in tokenized]
                    tensor = torch.LongTensor(indexed).to(device)
                    tensor = torch.LongTensor(indexed)
                    tensor = tensor.unsqueeze(0)
                    _, mid = model(tensor)
            
                    avg = {}
                    avg['2-grams'] = torch.mean(mid["Conved"][0], dim=-1)
                    avg['3-grams'] = torch.mean(mid["Conved"][1], dim=-1)
                    avg['4-grams'] = torch.mean(mid["Conved"][2], dim=-1)
            
                    for i in range(50):
                    
                        for j in range(3):
                            val = avg[str(j+2) + '-grams'][0][i]
                            if len(topK[str(j+2) + "-grams"][i]) < K:
                                topK[str(j+2) + "-grams"][i][val] = sentence
                            else:
                                min_i = min(topK[str(j+2) + "-grams"][i].keys())
                                if val > min_i:
                                    del topK[str(j+2) + "-grams"][i][min_i]
                                    topK[str(j+2) + "-grams"][i][val] = sentence         
                        
            if counter % 1000 == 0:
                print(counter, " sentences analysed")
    
    with open(FILE_PATH + '/top_K_sentences.pickle', 'wb') as f:
        pickle.dump(topK, f)
    
    return 





def get_concepts_from_topK_sentences(top_K, tokenizer):
    
    concepts = {}
    concepts["2-grams"] = [set() for f in range(50)]
    concepts["3-grams"] = [set() for f in range(50)]
    concepts["4-grams"] = [set() for f in range(50)]

    for i in range(50):
        
        for j in range(3):
            for s in top_K[str(j+2) + "-grams"][i]:
                sentence = top_K[str(j+2) + "-grams"][i][s]
                spacy_tokenized = tokenizer(sentence)
                tokens = [tok.text for tok in spacy_tokenized]
                if len(tokens) < (j+2):
                    tokens += ['<pad>'] * (j + 2 - len(tokens))
                    spacy_tokenized = Doc(spacy_tokenized.vocab, tokens)
                set_bigrams = set(textacy.extract.ngrams(spacy_tokenized, (j+2), filter_stops=False, filter_punct=False, filter_nums=False))
                concepts[str(j+2) + "-grams"][i].update(set_bigrams)
            
    return concepts


def create_synth_sentences(concepts):
    
    sentences = {}
    sentences["2-grams"] = [set() for f in range(50)]
    sentences["3-grams"] = [set() for f in range(50)]
    sentences["4-grams"] = [set() for f in range(50)]

    for i in range(50):
        
        for j in range(3):
            for c in concepts[str(j+2) + "-grams"][i]:
                sentences[str(j+2) + "-grams"][i].add((c.text + " ") * int((12/(j+2))))
            
    return sentences



def top_M_concepts(model, sentences, tokenizer, vocab, M = 3):
    '''Retrieve top-K activated sentences per filter '''
    
    # Turm off dropout while evaluating
    model.eval()
    topM = {}
    topM["2-grams"] = [{} for f in range(50)]
    topM["3-grams"] = [{} for f in range(50)]
    topM["4-grams"] = [{} for f in range(50)] 
    
    with torch.no_grad():
        
        for i in range(50):
            
            for j in range(3):
                
                for sentence in sentences[str(j+2) + "-grams"][i]:
                
                    concept = sentence[:int(len(sentence)/(12/(j+2)))-1]
                    tokenized = [tok.text for tok in tokenizer(sentence)]
                    indexed = [vocab.stoi[t] for t in tokenized]
                    # tensor = torch.LongTensor(indexed).to(pp.device)
                    tensor = torch.LongTensor(indexed)
                    tensor = tensor.unsqueeze(0)
                    _, mid = model(tensor)
            
                    avg = torch.mean(mid["Conved"][j][0][i]).item()
                
                    if len(topM[str(j+2) + "-grams"][i]) < M:
                        topM[str(j+2) + "-grams"][i][avg] = concept
                        
                    else:
                        min_i = min(topM[str(j+2) + "-grams"][i].keys())
                        if avg > min_i:
                            del topM[str(j+2) + "-grams"][i][min_i]
                            topM[str(j+2) + "-grams"][i][avg] = concept
            
                counter=0.0
                while len(topM[str(j+2) + "-grams"][i]) < M:
                    topM[str(j+2) + "-grams"][i][counter] = "\t"
                    counter += 0.000001
            
    return topM


def display_top_concepts(top_concepts):
    
    for j in range(3):
        print(str(j+2) + "-grams: ")
        for i in range(50):
            keys = list(top_concepts[str(j+2) + "-grams"][i].keys())
            print()
            print("Keys: ", keys)
            try: 
                c_1 = top_concepts[str(j+2) + "-grams"][i][keys[0]]
                c_2 = top_concepts[str(j+2) + "-grams"][i][keys[1]]
                c_3 = top_concepts[str(j+2) + "-grams"][i][keys[2]]
                print(i, ": ", c_1, ", ", c_2, ", ", c_3)
            except IndexError:
                print("No concepts")
        print()
    



