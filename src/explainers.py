import os

import numpy as np
import torch
import torch.nn as nn

import one_hot_encoders as ohe
from utils import lrp, visuals, num


CLASSES = ["World", "Sports", "Business", "Sci/Tech"]


class Explainer_CNN:
    
    def __init__(self, model, text, tokenizer, field_vocab, label_vocab, device, min_len = 5):
        
        self.model = model
        self.model.eval()
        self.text = text
        self.tokenized = [tok.text for tok in tokenizer(text)]
        if len(self.tokenized) < min_len:
            self.tokenized += ['<pad>'] * (min_len - len(self.tokenized))
            
        # Map words to word embeddings
        indexed = [field_vocab.stoi[t] for t in self.tokenized]
        tensor = torch.LongTensor(indexed).to(device)
        tensor = tensor.unsqueeze(0)
        self.input_size = tensor.size()[1]
        
        # Get predictions
        output, mid = self.model(tensor)
        self.embedded = mid["Embedded"]
        if output.shape[1] == 1:
            self.task = 'Binary'
            score = (torch.sigmoid(output)).item()
            self.label = label_vocab.itos[round(score)]
        else:
            self.task = 'Multi'
            score = torch.max(output.data, 1)
            self.label = CLASSES[int(label_vocab.itos[score[1]]) - 1]
        
        self.relevances = {}
        self.max = 0
        self.min = 0
        
        for i in range(output.shape[1]):
        
            relevance = {}
            linear = torch.full(output.shape, 0, dtype=torch.float)
            linear[0][i] = output[0][i]
            relevance["Baseline"]  = linear
            relevance["Linear bias"] = self.model.linear.bias[i]
            next_R = relevance["Baseline"]
            
            if hasattr(self.model, "conv"):
                maxpooled_2_R = lrp.regular(mid["Pooled data 2"].squeeze(2), self.model.linear, next_R)
                relevance["Conved 2"] = lrp.maxpool(mid["Conved 2"], mid["Pooled indices 2"], maxpooled_2_R)
                relevance["Conved 2 bias"] = lrp.bias(self.model.conv.bias, mid["Conved 2"], relevance["Conved 2"])
                next_R = relevance["Conved 2"]
                maxpool_R = lrp.regular(torch.cat(mid["Pooled data"], dim = 1).unsqueeze(1), self.model.conv, relevance["Conved 2"])
                split_next_R = torch.split(maxpool_R.squeeze(0), 50, dim=1)
            else:
                maxpool_R = lrp.regular(torch.cat(mid["Pooled data"], dim = 1), self.model.linear, next_R)
                split_next_R = torch.split(maxpool_R, 50, dim=1)
            
            split_embed_R = []
            
            for j in range(len(split_next_R)):
                relevance[str(j+2) + "-grams"] = lrp.maxpool(mid["Conved"][j], mid["Pooled indices"][j], split_next_R[j])
                split_embed_R.append(lrp.regular(mid["Embedded"], self.model.convs[j], relevance[str(j+2)+"-grams"].unsqueeze(3)))
                relevance[str(j+2)+"-grams bias"] = lrp.bias(self.model.convs[j].bias, mid["Conved"][j], relevance[str(j+2)+"-grams"])
            
            relevance["Embedded"] = torch.zeros(split_embed_R[0].shape)
            for R in split_embed_R:
                relevance["Embedded"] += R
                
            g_max, g_min = num.get_max_and_min(relevance)
            if g_max > self.max:
                self.max = g_max
            if g_min < self.min:
                self.min = g_min
            
            if self.task == 'Multi':
                self.relevances[CLASSES[int(label_vocab.itos[i]) - 1]] = relevance
                
            elif self.task == 'Binary':
                self.relevances = relevance
        
        if self.task == 'Multi':
            self.get_comp_rel()
            self.get_net_rel()
        
        self.normalise()
        
        if hasattr(self.model, "conv"):
            self.arg = ohe.OneHot_DeeperCNN(self.input_size, mid['Pooled indices'])
        else:
            self.arg = ohe.OneHot_ShallowCNN(self.input_size)
    


    def get_comp_rel(self):

        pred = self.relevances[self.label]
        keys = pred.keys()
        self.comp_rel = {}
        
        for CLASS in self.relevances:
            
            if CLASS != self.label:
                
                alt = self.relevances[CLASS]
                relevance = {}
                
                for k in keys:
                    relevance[k] = pred[k] - alt[k]
                    this_max = torch.max(relevance[k]).item()
                    this_min = torch.min(relevance[k]).item()
                    if this_max > self.max:
                        self.max = this_max
                    if this_min < self.min:
                        self.min = this_min
                    
                    if k in ("2-grams", "3-grams", "4-grams", "Conved 2"):
                        collaped = torch.sum(relevance[k], dim = 1, keepdim=True)
                        collapsed_max = torch.max(collaped).item()
                        collapsed_min = torch.min(collaped).item()
                        if collapsed_max > self.max:
                            self.max = collapsed_max
                        if collapsed_min < self.min:
                            self.min = collapsed_min    
                    
                    elif k == "Embedded":
                        collaped = torch.sum(relevance[k], dim = 3)
                        collapsed_max = torch.max(collaped).item()
                        collapsed_min = torch.min(collaped).item()
                        if collapsed_max > self.max:
                            self.max = collapsed_max
                        if collapsed_min < self.min:
                            self.min = collapsed_min                          
                        
                self.comp_rel[self.label + " > " + CLASS] = relevance


    def get_net_rel(self):
        
        dicts = list(self.comp_rel.keys())
        keys = list(self.comp_rel[dicts[0]])
        self.net_rel = {}

        for k in keys:
            self.net_rel[k] = sum(self.comp_rel[d][k] for d in dicts)
 
            this_max = torch.max(self.net_rel[k]).item()
            this_min = torch.min(self.net_rel[k]).item()
            if this_max > self.max:
                self.max = this_max
            if this_min < self.min:
                self.min = this_min
            
            if k in ("2-grams", "3-grams", "4-grams", "Conved 2"):
                collaped = torch.sum(self.net_rel[k], dim = 1, keepdim=True)
                collapsed_max = torch.max(collaped).item()
                collapsed_min = torch.min(collaped).item()
                if collapsed_max > self.max:
                    self.max = collapsed_max
                if collapsed_min < self.min:
                    self.min = collapsed_min   
                    
            elif k == "Embedded":
                collaped = torch.sum(self.net_rel[k], dim = 3)
                collapsed_max = torch.max(collaped).item()
                collapsed_min = torch.min(collaped).item()
                if collapsed_max > self.max:
                    self.max = collapsed_max
                if collapsed_min < self.min:
                    self.min = collapsed_min                   
 


    def normalise(self):
        g_max = max(abs(self.max), abs(self.min))
        if self.task == 'Binary':
            keys = list(self.relevances.keys())
            self.relevances = {k: self.relevances[k]/g_max for k in keys} 
        elif self.task == 'Multi':
            keys = list(self.net_rel.keys())
            self.net_rel = {k: self.net_rel[k]/g_max for k in keys}
            
            # dicts = list(self.comp_rel.keys())
            # self.comp_rel = {d: {k: self.comp_rel[d][k]/g_max for k in keys} for d in dicts}
            
            dicts = list(self.comp_rel.keys())
            self.comp_rel = {d: {k: self.comp_rel[d][k]/3 for k in keys} for d in dicts}
            
            raw_max, raw_min = num.get_max_and_min(self.relevances[self.label])
            g_raw_max = max(abs(raw_min), abs(raw_max))
            self.relevances[self.label] = {k: self.relevances[self.label][k]/g_raw_max for k in keys}



    def explain_with_groups_of_ngrams(self, dest_file, ALT = None):

        arg = torch.sum(self.arg.args['Conved 2'], dim = 1, keepdim=True)
        title_temp = '<h1 style="color: black; text-align: center">{}</h1>'
        subtitle_temp = '<h2 style="color: black; text-align: center">{}</h2>'

        if self.task == 'Binary':
            rel = self.relevances['Conved 2']
            subtitle = self.label + ": Conved 2"
        elif ALT == None:
            rel = self.net_rel['Conved 2']
            subtitle = self.label + "* : Conved 2"
        else:
            rel = self.comp_rel[self.label + " > " + ALT]['Conved 2']
            subtitle = "Comp " + self.label + " and " + ALT + ": Conved 2"

        with open(os.path.join(dest_file, "ngram_groups.html"), 'w') as f:
            f.write(title_temp.format(self.text))
            f.write(subtitle_temp.format(subtitle))

        rel = torch.sum(rel, dim = 1, keepdim=True)

        for g in range(self.input_size + 1):

            strength = rel[0][0][g] * arg[0][0][g]
            strength = strength.detach().numpy()[:self.input_size]

            if not np.array_equal(strength, np.full(strength.shape, 0.0)):
                with open(os.path.join(dest_file, "ngram_groups.html"), 'w') as f:
                    f.write(subtitle_temp.format(subtitle))
                    s = visuals.colorize(strength, self.tokenized)
                    f.write(s)
                
        

        
    def explain_with_ngrams(self, ALT = None):      # OK
        
        if self.task == 'Binary':
            rel = self.relevances
        elif ALT == None:
            rel = self.net_rel
        else:
            rel = self.comp_rel[self.label + " > " + ALT]
        
        bi_strength = torch.sum(rel["2-grams"], dim = 1)
        pad_bi = torch.full([1,(self.input_size - bi_strength.shape[1])], 0, dtype=torch.float)
        bi_strength = torch.cat((bi_strength, pad_bi), dim=1)
        
        tri_strength = torch.sum(rel["3-grams"], dim = 1)
        pad_tri = torch.full([1,(self.input_size - tri_strength.shape[1])], 0, dtype=torch.float)
        tri_strength = torch.cat((tri_strength, pad_tri), dim=1)
        
        quad_strength = torch.sum(rel["4-grams"], dim = 1)
        pad_quad = torch.full([1,(self.input_size - quad_strength.shape[1])], 0, dtype=torch.float)
        quad_strength = torch.cat((quad_strength, pad_quad), dim=1)
        
        strength = torch.stack((bi_strength, tri_strength, quad_strength))
        strength = strength.squeeze(1)
        strength = strength.detach().numpy()
        
        y_labels = ["Quad-grams", "Tri-grams", "Bi-grams"]
        visuals.show_explanation(strength, y_labels, self.tokenized, self.text)

        
     
    def explain_with_filters(self, ALT = None): # OK

        if self.task == 'Binary':
            rel = self.relevances
        elif ALT == None:
            rel = self.net_rel
        else:
            rel = self.comp_rel[self.label + " > " + ALT]
        
        bi_strength = torch.sum(rel["2-grams"], dim = 2)
        tri_strength = torch.sum(rel["3-grams"], dim = 2)
        quad_strength = torch.sum(rel["4-grams"], dim = 2)
        
        strength = torch.stack((bi_strength, tri_strength, quad_strength))
        strength = strength.squeeze(1)
        strength = strength.detach().numpy()
        
        y_labels = ["Quad-gram filters", "Tri-gram filters", "Bi-gram filters"]
        visuals.show_explanation(strength, y_labels, range(50), self.text)      
        
     
        
    def explain_with_words(self, ALT = None, render = 'plot', raw=False): # OK
        
        if self.task == 'Binary':
            rel = self.relevances
        elif raw:
            rel = self.relevances[self.label]
        elif ALT == None:
            rel = self.net_rel
        else:
            rel = self.comp_rel[self.label + " > " + ALT]

        input_R = torch.sum(rel["Embedded"], dim=3).squeeze(0)
        
        if render == 'plot':
            numpy_array = input_R.detach().numpy()
            visuals.show_explanation(numpy_array, ["Words"], self.tokenized, self.text)  
            
        elif render == 'html':
            numpy_array = input_R.squeeze(0).detach().numpy()  
            colored_string = visuals.colorize(numpy_array, self.tokenized) 
            return colored_string            



    def explain_with_word_embedding_features(self, ALT = None): #%% TRY OUT WITH TERMINAL SCRIPT

        if self.task == 'Binary':
            rel = self.relevances
        elif ALT == None:
            rel = self.net_rel
        else:
            rel = self.comp_rel[self.label + " > " + ALT]  

        input_R = torch.sum(rel["Embedded"], dim=2).squeeze(0)
        numpy_array = input_R.detach().numpy()
        visuals.show_explanation(numpy_array, ["Word features"], range(100), self.text)        




    def explain_ngram_with_words(self, N, INDEX, ALT = None, render = 'plot'):
        
        if not N in (2,3,4):
            print("Invalid value for N: needs to be one of 2, 3 or 4.")
            return
        if INDEX >= self.input_size or INDEX < 0:
            print("Invalid value for INDEX: for this input text INDEX needs to be a number between 0 and ", self.input_size - 1, " included.")
            return

        if self.task == 'Binary':
            rel = self.relevances[str(N)+"-grams"]
        elif ALT == None:
            rel = self.net_rel[str(N)+"-grams"]
        else:
            rel = self.comp_rel[self.label + " > " + ALT][str(N)+"-grams"]

        mask = torch.full(rel.shape, 0, dtype=torch.float)
        for i in range(rel.shape[1]):
            mask[0][i][INDEX] = 1.0

        masked_rel = torch.mul(mask, rel)
        cond_rel = lrp.regular(self.embedded, self.model.convs[N-2], masked_rel.unsqueeze(3))
        cond_rel = torch.sum(cond_rel, dim=3).squeeze(0)
        cond_rel = cond_rel.detach().numpy()
        
        if render == 'plot':
            visuals.show_explanation(cond_rel[0:1, INDEX:INDEX+N], ["Words"], self.tokenized[INDEX:INDEX+N], self.tokenized[INDEX:INDEX+N]) 
        elif render == 'html':
            if ALT != None:
                cond_rel = (-1) * cond_rel
            colored_string = visuals.colorize(cond_rel[0][INDEX:INDEX+N], self.tokenized[INDEX:INDEX+N])
            return colored_string




    def explain_ngram_with_filters(self, N, INDEX, concepts = None, ALT = None, partial = False, largest=True): # OK

        if self.task == 'Binary':
            rel = self.relevances[str(N)+"-grams"]
        elif ALT == None:
            rel = self.net_rel[str(N)+"-grams"]
        else:
            rel = self.comp_rel[self.label + " > " + ALT][str(N)+"-grams"]
        
        sliced = rel[0, :, INDEX]
        
        if concepts == None:
            strength = sliced.unsqueeze(0).detach().numpy()
            visuals.show_explanation(strength, ["Filters"], range(50), self.tokenized[INDEX:INDEX+N])
            return
            
        if partial:
            sliced = torch.clamp(sliced, min=0.0)
        dec_order = torch.topk(sliced, sliced.shape[0], largest=largest, sorted=True)
        values = dec_order[0]
        indices = dec_order[1]

        filters = []

        for i in range(len(values)):

            if values[i].item() != 0.0:
                filter_nb = indices[i].item()
                value = values[i].detach().numpy()
                if ALT != None:
                    value = (-1) * value
                colored_string = visuals.colorize_filter(str(N), filter_nb, value, list(concepts[str(N)+"-grams"][filter_nb].values()))
                filters.append(colored_string)

        return filters      
 





    def get_ngrams_from_words(self, INDEX, concepts, ALT=None): # IMPORTAMT

        if INDEX >= self.input_size or INDEX < 0:
            print("Invalid value for INDEX: for this input text INDEX needs to be a number between 0 and ", self.input_size - 1, " included.")
            return

        if self.task == 'Binary':
            rel = self.relevances
        elif ALT == None:
            rel = self.net_rel
        else:
            rel = self.comp_rel[self.label + " > " + ALT]

        ngrams = []
        INDEX = int(INDEX)

        for i in range(3):
            N = i + 2
            shift = i + 1
            strength = torch.sum(rel[str(N) + "-grams"], dim = 1).squeeze(0)
            index = list(range(INDEX-shift, INDEX + 1 ))
            index = [x for x in index if x >= 0]
            index = [x for x in index if x < (self.input_size - shift)]
            for j in index:
                s = strength[j].item()
                if s != 0.0:
                    largest = (s > 0.0)
                    ngrams.append(self.get_arg(N, j, concepts, ALT, largest = largest))

        return ngrams






    def explain_with_single_ngram(self, N, INDEX, ALT=None): # IMPORTANT

        if not N in (2,3,4):
            print("Invalid value for N: needs to be one of 2, 3 or 4.")
            return
        if INDEX >= self.input_size or INDEX < 0:
            print("Invalid value for INDEX: for this input text INDEX needs to be a number between 0 and ", self.input_size - 1, " included.")
            return

        if self.task == 'Binary':
            rel = self.relevances
        elif ALT == None:
            rel = self.net_rel
        else:
            rel = self.comp_rel[self.label + " > " + ALT]

        arg = torch.sum(self.arg.args[str(N)+"-grams"], dim = 1)[0][INDEX]
        arg[arg!=0] = 1
        strength = torch.sum(rel[str(N)+"-grams"], dim=1)[0][INDEX] * arg
        strength = strength.detach().numpy()
        if ALT != None:
            strength = (-1) * strength
        colored_string = visuals.colorize(strength, self.tokenized)

        return colored_string




    def top_ngrams(self, concepts, ALT=None): # IMPORTANT

        ngrams = []
        if ALT == None:
            rel = self.net_rel
        else:
            rel = self.comp_rel[self.label + " > " + ALT]

        bi_strength = torch.clamp(torch.sum(rel["2-grams"], dim = 1), min=0).squeeze(0)
        bi_length = len(bi_strength)
        tri_strength = torch.clamp(torch.sum(rel["3-grams"], dim = 1), min=0).squeeze(0)
        tri_length = len(tri_strength)
        quad_strength = torch.clamp(torch.sum(rel["4-grams"], dim = 1), min=0).squeeze(0)
        # quad_length = len(quad_strength)

        strength = torch.cat([bi_strength, tri_strength, quad_strength])
        result = torch.topk(strength, strength.shape[0], sorted=True)
        values = result[0]
        indices = result[1]

        for e in range(len(values)):
            if values[e].item() <= 0.0:
                break

            if indices[e] < bi_length:
                N = 2
                INDEX = indices[e].item()
            elif indices[e] < (bi_length + tri_length):
                N = 3
                INDEX = indices[e].item() - bi_length
            else:
                N = 4
                INDEX = indices[e].item() - bi_length - tri_length

            arg = self.get_arg(N, INDEX, concepts, ALT, partial=True)
            ngrams.append(arg)

        return ngrams




      

    def get_arg(self, N, INDEX, concepts, ALT = None, partial=False, largest = True): #IMPORTANT

        if not N in (2,3,4):
            print("Invalid value for N: needs to be one of 2, 3 or 4.")
            return
        if INDEX >= self.input_size or INDEX < 0:
            print("Invalid value for INDEX: for this input text INDEX needs to be a number between 0 and ", self.input_size - 1, " included.")
            return

        colored_string = self.explain_with_single_ngram(N, INDEX, ALT)
        words = self.explain_ngram_with_words(N, INDEX, ALT, render='html')
        filters = self.explain_ngram_with_filters(N, INDEX, concepts, ALT, partial, largest)
        arg = {"N": N, "Index": INDEX, "Html": colored_string, "Words": words, "Filters": filters}

        return arg
    
 
    def show_output(self):
        print("'", self.text, "'")
        print("Label: ", self.label) 
        
        
        
 
        
 
class CustomLayer(nn.Module):
    def __init__(self, input_size, hidden_size):
        super(CustomLayer, self).__init__()
        self.linear_input = nn.Linear(input_size, hidden_size)
        self.linear_hidden = nn.Linear(hidden_size, hidden_size)
    
    def forward(self, x, h, r):
        Wx_x_plus_b = self.linear_input(x)
        Wh_h_plus_b = self.linear_hidden(h)
        return Wx_x_plus_b + r*Wh_h_plus_b



class Explainer_RNN():
    def __init__(self, model, title, tokenizer, field_vocab, label_vocab, device, min_len = 5):
        
        self.model = model
        self.model.eval()
        
        self.title = title
        self.tokenized = [tok.text for tok in tokenizer(title)]
        if len(self.tokenized) < min_len:
            self.tokenized += ['<pad>'] * (min_len - len(self.tokenized))
            
        # Map words to word embeddings
        indexed = [field_vocab.stoi[t] for t in self.tokenized]
        tensor = torch.LongTensor(indexed).to(device)
        tensor = tensor.unsqueeze(0)
        self.input_size = tensor.size()[1]
        
        hidden = self.model.initHidden()
        output, h_seq, self.embedded = self.model(tensor, hidden)

        if output.shape[2] == 1:
            self.task = 'Binary'
            score = (torch.sigmoid(output)).item()
            self.label = label_vocab.itos[round(score)]
        else:
            self.task = 'Multi'
            score = torch.max(output.data, 2)
            self.label = CLASSES[int(label_vocab.itos[score[1]]) - 1]
        
        self.get_weights_and_biases()
        self.relevances = {}
                
        for i in range(output.shape[2]):
            
            relevance = {}
            relevance["Word strength"] = torch.full((1, h_seq.shape[1]), 0)
            linear = torch.full(output.shape, 0)
            linear[0][0][i] = output[0][0][i]
            relevance["Linear"]  = linear
            
            relevance["GRU output"], s_gru = lrp.regular_for_rnn(h_seq[0][-1], self.model.linear, relevance["Linear"])
            relevance["GRU output"] += (self.model.linear.bias*(s_gru.squeeze(0).squeeze(0)) / 128.0).unsqueeze(1).expand(-1, 128).sum(0).squeeze(0)
            
            last_R = relevance["GRU output"]
            
            for j in reversed(range(h_seq.shape[1])):
                
                x = self.embedded[0][j]
                h = None
                if j == 0:
                    h = self.model.initHidden().squeeze(0).squeeze(0)
                else: 
                    h = h_seq[0][j-1]
                z = self.get_update_gate(x, h)
                r = self.get_reset_gate(x, h)

                # relevance["GRU h_t_minus_1 " + str(j)] = z*last_R
                relevance["GRU h_t_minus_1 INTER " + str(j)] = z*last_R
                relevance["GRU n_t " + str(j)] = (1-z)*last_R
                
                R_x, R_h, s_xh = self.get_relevance_custom(x, h, r, relevance["GRU n_t " + str(j)])
                relevance["GRU x_t " + str(j)] = R_x
                relevance["GRU x_t " + str(j)] += (self.biases["Output input"]*(s_xh.squeeze(0)) / 100.0).unsqueeze(1).expand(-1,100).sum(0).squeeze(0)
                relevance["GRU h_t_minus_1 " + str(j)] = relevance["GRU h_t_minus_1 INTER " + str(j)] + R_h
                relevance["GRU h_t_minus_1 " + str(j)] += (self.biases["Output hidden"]*r.squeeze(0)*(s_xh.squeeze(0)) / 128.0).unsqueeze(1).expand(-1,128).sum(0).squeeze(0)
                
                last_R = relevance["GRU h_t_minus_1 " + str(j)]
                
                relevance["Word strength"][0][j] = relevance["GRU x_t " + str(j)].sum()
            
            if self.task == 'Multi':
                self.relevances[CLASSES[int(label_vocab.itos[i]) - 1]] = relevance
                
            elif self.task == 'Binary':
                self.relevances = relevance
        
        if self.task == 'Multi':
            self.get_comp_rel()
            self.get_net_rel()
        
        # self.normalise()

    
    def get_comp_rel(self):

        pred = self.relevances[self.label]
        keys = pred.keys()
        self.comp_rel = {}
        
        for CLASS in self.relevances:
            
            if CLASS != self.label:
                
                alt = self.relevances[CLASS]
                relevance = {k: pred[k] - alt[k] for k in keys}                      
                self.comp_rel[self.label + " > " + CLASS] = relevance


    def get_net_rel(self):
        
        dicts = list(self.comp_rel.keys())
        keys = list(self.comp_rel[dicts[0]])
        self.net_rel = {k: sum(self.comp_rel[d][k] for d in dicts)/3 for k in keys}

    
    def normalise(self):

        max_val = 0
        min_val = 0
        
        if self.task == 'Binary':
            keys = list(self.relevances.keys())
            for k in keys:
                
                this_max = torch.max(self.relevances[k]).item()
                this_min = torch.min(self.relevances[k]).item()
            
                if this_max > max_val:
                    max_val = this_max
                
                if this_min < min_val:
                    min_val = this_min                
                
            self.relevances = {k: self.relevances[k]/max_val for k in keys} 
        elif self.task == 'Multi':
            keys = list(self.net_rel.keys())
            for k in keys:
                
                this_max = torch.max(self.net_rel[k]).item()
                this_min = torch.min(self.net_rel[k]).item()
            
                if this_max > max_val:
                    max_val = this_max
                
                if this_min < min_val:
                    min_val = this_min  
            
            dicts = list(self.comp_rel.keys())
            keys_2 = list(self.comp_rel[dicts[0]].keys())
            
            for d in dicts:
                for k_2 in keys_2:
                    
                    this_max = torch.max(self.net_rel[k]).item()
                    this_min = torch.min(self.net_rel[k]).item()
            
                    if this_max > max_val:
                        max_val = this_max
                
                    if this_min < min_val:
                        min_val = this_min
            
            self.net_rel = {k: self.net_rel[k]/max_val for k in keys}
            self.comp_rel = {d: {k_3: self.comp_rel[d][k_3]/max_val for k_3 in keys_2} for d in dicts}
    
    
    def get_relevance_custom(self, x, h, r, next_R):
        
        layer = CustomLayer(x.shape[0], h.shape[0])
        layer.linear_input.weight.data.copy_(self.weights["Output input"])
        layer.linear_input.bias.data.copy_(self.biases["Output input"])
        layer.linear_hidden.weight.data.copy_(self.weights["Output hidden"])
        layer.linear_hidden.bias.data.copy_(self.biases["Output hidden"])
        
        x = (x.data).requires_grad_(True)
        h = (h.data).requires_grad_(True)
        n = layer.forward(x,h,r.data)
        s = next_R/(n + 1e-9)
        (n*s.data).sum().backward()
        c_x = x.grad
        R_x = x*c_x
        c_h = h.grad
        R_h = h*c_h
        
        return R_x, R_h, s
        
    
    def get_update_gate(self, x, h):
        
        linear_input = nn.Linear(x.shape[0], h.shape[0])
        linear_hidden = nn.Linear(h.shape[0], h.shape[0])
        linear_input.weight.data.copy_(self.weights['Update input'])
        linear_input.bias.data.copy_(self.biases["Update input"])
        linear_hidden.weight.data.copy_(self.weights['Update hidden'])
        linear_hidden.bias.data.copy_(self.biases["Update hidden"])        
        
        Wx_x_plus_bx = linear_input(x)
        Wh_h_plus_bh = linear_hidden(h)
        result = torch.sigmoid(Wx_x_plus_bx + Wh_h_plus_bh)

        return result
    
    
    def get_reset_gate(self, x, h):
        
        linear_input = nn.Linear(x.shape[0], h.shape[0])
        linear_hidden = nn.Linear(h.shape[0], h.shape[0])
        linear_input.weight.data.copy_(self.weights['Reset input'])
        linear_input.bias.data.copy_(self.biases["Reset input"])
        linear_hidden.weight.data.copy_(self.weights['Reset hidden'])
        linear_hidden.bias.data.copy_(self.biases["Reset hidden"])        
        
        Wx_x_plus_bx = linear_input(x)
        Wh_h_plus_bh = linear_hidden(h)
        result = torch.sigmoid(Wx_x_plus_bx + Wh_h_plus_bh)

        return result
    
    def explain_output(self, ALT = None):
        if self.task == 'Binary':
            rel = self.relevances[self.label]["Word strength"]
        elif ALT == None:
            rel = self.net_rel["Word strength"]
        else:
            rel = self.comp_rel[self.label + " > " + ALT]["Word strength"]
        numpy_array = rel.detach().numpy()
        print(numpy_array)
        visuals.show_explanation(numpy_array, ["Words"], self.tokenized, self.title) 
        
        

    def get_weights_and_biases(self):
        
        self.weights = {}
        self.biases = {}
        
        input_weights = self.model.gru.weight_ih_l0

        self.weights["Reset input"] = input_weights[:self.model.hidden_size]
        self.weights["Update input"] = input_weights[self.model.hidden_size: 2*self.model.hidden_size]
        self.weights["Output input"] = input_weights[2*self.model.hidden_size: 3*self.model.hidden_size]
        
        hidden_weights = self.model.gru.weight_hh_l0
        
        self.weights["Reset hidden"] = hidden_weights[:self.model.hidden_size]
        self.weights["Update hidden"] = hidden_weights[self.model.hidden_size: 2*self.model.hidden_size]
        self.weights["Output hidden"] = hidden_weights[2*self.model.hidden_size: 3*self.model.hidden_size]
        
        input_biases = self.model.gru.bias_ih_l0
        
        self.biases["Reset input"] = input_biases[:self.model.hidden_size]
        self.biases["Update input"] = input_biases[self.model.hidden_size: 2*self.model.hidden_size]
        self.biases["Output input"] = input_biases[2*self.model.hidden_size: 3*self.model.hidden_size]
        
        hidden_biases = self.model.gru.bias_hh_l0
        
        self.biases["Reset hidden"] = hidden_biases[:self.model.hidden_size]
        self.biases["Update hidden"] = hidden_biases[self.model.hidden_size: 2*self.model.hidden_size]
        self.biases["Output hidden"] = hidden_biases[2*self.model.hidden_size: 3*self.model.hidden_size]

    def show_output(self):
        print("'", self.title, "'")
        print("Label: ", self.label)         




class Explainer_VDCNN():
    
    def __init__(self, model, text, field_vocab, label_vocab, device, fix_len = 1024):
        
        self.model = model
        self.model.eval()
        self.text = text
        # self.descr = descr
        self.tokenized = list(self.descr)
        if len(self.tokenized) < fix_len:
            self.tokenized += ['<pad>'] * (fix_len - len(self.tokenized))
        indexed = [field_vocab.stoi[t] for t in self.tokenized]
        tensor = torch.LongTensor(indexed).to(device)
        tensor = tensor.unsqueeze(0)
        self.input_size = tensor.size()[1]
        
        output, mid = self.model(tensor)
        
        if output.shape[1] == 1:
            self.task = 'Binary'
            score = (torch.sigmoid(output)).item()
            self.label = label_vocab.itos[round(score)]
        else:
            self.task = 'Multi'
            score = torch.max(output.data, 1)
            self.label = CLASSES[int(label_vocab.itos[score[1]]) - 1]
        # score = torch.max(output.data, 1)
        # self.label = CLASSES[int(label_vocab.itos[score[1]]) - 1]
        
        self.relevances = {}
        self.max = 0
        self.min = 0
        
        for i in range(output.shape[1]):
            
            relevance = {}
            
            # Get baseline relevance (directly from the output for this class)
            linear = torch.full(output.shape, 0)
            linear[0][i] = output[0][i]
            relevance["Baseline"]  = linear
            
            # Get the relevance of the biases in the 3 linear layers in model.fc
            inputs = [mid["Top-K"]]
            
            for layer in self.model.fc:
                z = layer(inputs[-1])
                inputs.append(z)
            
            relevance["Linear biases 3"] = lrp.bias(self.model.fc[4].bias, mid["Output"], relevance["Baseline"])
            next_R = lrp.regular(inputs[4], self.model.fc[4], relevance["Baseline"])
            relevance["Linear biases 2"] = lrp.bias(self.model.fc[2].bias, inputs[4], next_R)
            next_R = lrp.regular(inputs[2], self.model.fc[2], next_R)
            relevance["Linear biases 1"] = lrp.bias(self.model.fc[0].bias, inputs[2], next_R)

            # As intermediate calculation, get relevance of maxpooled elements
            k_maxpool_R = lrp.regular(mid["Top-K"], self.model.fc, relevance["Baseline"])
            k_maxpool_R = k_maxpool_R.view(mid["Top-K indices"].shape)
            
            # Get relevance of each res_block
            self.counter = len(self.model.res_blocks)
            next_R = lrp.maxpool_2(mid["Resed"], mid["Top-K indices"], k_maxpool_R)


            for res_block in reversed(self.model.res_blocks):
                relevance["Res block " + str(self.counter)] = next_R
                dic, next_R = self.get_R_res_block(res_block.mid["Input"], res_block, next_R)
                relevance.update(dic)
                self.counter -= 1
            
            relevance["1st conv"] = next_R
            # relevance["Embedded"] = lrp.regular(mid["Transposed"], self.model.conv, next_R, eps=EPS*torch.std(mid["Transposed"].data))
            relevance["Embedded"] = lrp.alpha_beta(mid["Transposed"], self.model.conv, next_R)
            relevance["1st conv biases"] = lrp.bias(self.model.conv.bias, mid["Conved"], next_R)
        
            g_max, g_min = num.get_max_and_min(relevance)
            
            if g_max > self.max:
                self.max = g_max
            if g_min < self.min:
                self.min = g_min
        
            if self.task == 'Multi':
                self.relevances[CLASSES[int(label_vocab.itos[i]) - 1]] = relevance
            elif self.task == 'Binary':
                self.relevances = relevance
        
        if self.task == 'Multi':
            self.get_comp_rel()
            self.get_net_rel()
        
        self.normalise()


    def get_R_conv_block(self, z, block, next_R, output=None):
        
        relevance = {}
        relevance["Res block " + str(self.counter) + " conv 2"] = next_R
        inputs = [z]
        for layer in block.sequential:
            z = layer(inputs[-1])
            inputs.append(z)
        
        R1 = lrp.alpha_beta(inputs[3], block.sequential[3], next_R)
        R2 = lrp.alpha_beta(inputs[0], block.sequential[0], R1)
        
        biases_2 = block.sequential[3].bias
        relevance["Res block " + str(self.counter) + " conv 2 biases"] = lrp.bias(biases_2, inputs[4], next_R)
        biases_1 = block.sequential[0].bias
        relevance["Res block " + str(self.counter) + " conv 1 biases"] = lrp.bias(biases_1, inputs[1], R1)
        
        relevance["Res block " + str(self.counter) + " conv 1"] = R1
        
        return relevance, R2



    def get_R_res_block(self, a, block, next_R):

        mid = block.mid
        relevance = {}
        relevance["Res block " + str(self.counter) + " shortcut"] = next_R * (mid["Shortcut"]/mid["Output"])
        
        input_R = lrp.alpha_beta(a, block.shortcut, relevance["Res block " + str(self.counter) + " shortcut"])        
        relevance["Res block " + str(self.counter) + " shortcut biases"] = lrp.bias(block.shortcut.bias, mid["Shortcut"], relevance["Res block " + str(self.counter) + " shortcut"])
        
        if block.pool:
            R = next_R * (mid["Pooled"]/mid["Output"])
            maxpool_R = lrp.regular(mid["Conved"], block.pool, R)
            dic, x_R = self.get_R_conv_block(a, block.conv_block, maxpool_R)
            
        else:
            R = next_R * (mid["Conved"]/mid["Output"])
            dic, x_R = self.get_R_conv_block(a, block.conv_block, R)
        
        relevance.update(dic)
        next_R = input_R + x_R
        
        return relevance, next_R
    


    def get_comp_rel(self):

        pred = self.relevances[self.label]
        keys = pred.keys()
        self.comp_rel = {}
        
        for CLASS in self.relevances:
            
            if CLASS != self.label:
                
                alt = self.relevances[CLASS]
                relevance = {}
                
                for k in keys:
                    relevance[k] = pred[k] - alt[k]
                    this_max = torch.max(relevance[k]).item()
                    this_min = torch.min(relevance[k]).item()
                    if this_max > self.max:
                        self.max = this_max
                    if this_min < self.min:
                        self.min = this_min
                    
                    if k in ("2-grams", "3-grams", "4-grams", "Conved 2"):
                        collaped = torch.sum(relevance[k], dim = 1, keepdim=True)
                        collapsed_max = torch.max(collaped).item()
                        collapsed_min = torch.min(collaped).item()
                        if collapsed_max > self.max:
                            self.max = collapsed_max
                        if collapsed_min < self.min:
                            self.min = collapsed_min    
                    
                    elif k == "Embedded":
                        collaped = torch.sum(relevance[k], dim = 3)
                        collapsed_max = torch.max(collaped).item()
                        collapsed_min = torch.min(collaped).item()
                        if collapsed_max > self.max:
                            self.max = collapsed_max
                        if collapsed_min < self.min:
                            self.min = collapsed_min                          
                        
                self.comp_rel[self.label + " > " + CLASS] = relevance


    def get_net_rel(self):
        
        dicts = list(self.comp_rel.keys())
        keys = list(self.comp_rel[dicts[0]])
        self.net_rel = {}

        for k in keys:
            self.net_rel[k] = sum(self.comp_rel[d][k] for d in dicts)
 
            this_max = torch.max(self.net_rel[k]).item()
            this_min = torch.min(self.net_rel[k]).item()
            if this_max > self.max:
                self.max = this_max
            if this_min < self.min:
                self.min = this_min

            if k == "Embedded":
                collaped = torch.sum(self.net_rel[k], dim = 3)
                collapsed_max = torch.max(collaped).item()
                collapsed_min = torch.min(collaped).item()
                if collapsed_max > self.max:
                    self.max = collapsed_max
                if collapsed_min < self.min:
                    self.min = collapsed_min  
                    
            elif not "biases" in k:
                collaped = torch.sum(self.net_rel[k], dim = 1, keepdim=True)
                collapsed_max = torch.max(collaped).item()
                collapsed_min = torch.min(collaped).item()
                if collapsed_max > self.max:
                    self.max = collapsed_max
                if collapsed_min < self.min:
                    self.min = collapsed_min   
 


    def normalise(self):
        g_max = max(abs(self.max), abs(self.min))
        if self.task == 'Binary':
            dicts = list(self.relevances.keys())
            keys = keys = list(self.relevances[dicts[0]].keys())
            self.relevances = {d: {k: self.relevances[k]/g_max for k in keys} for d in dicts}
        elif self.task == 'Multi':
            keys = keys = list(self.net_rel.keys())
            self.net_rel = {k: self.net_rel[k]/g_max for k in keys}
            
            dicts = list(self.comp_rel.keys())
            self.comp_rel = {d: {k: self.comp_rel[d][k]/g_max for k in keys} for d in dicts}
        
        
        
    def explain_with_chars(self, dest_file, CLASS = None):
        
        title_temp = '<h1 style="color: black; text-align: center">{}</h1>'
        subtitle_temp = '<h2 style="color: black; text-align: center">{}</h2>'
        with open(os.path.join(dest_file, "input_chars.html"), 'w') as f:
            f.write(title_temp.format(self.title))
        
            if CLASS == None: 
                strength = torch.sum(self.net_rel["Embedded"], dim=1)
                # strength = strength.detach().numpy()[0][:self.input_len]
                strength = strength.detach().numpy()[0]
                subtitle = self.label + "* : individual characters"
            
            else: 
                strength = torch.sum(self.comp_rel[self.label + " > " + CLASS]["Embedded"], dim=1)
                # strength = strength.detach().numpy()[0][:self.input_len]
                strength = strength.detach().numpy()[0]
                subtitle = "Comp " + self.label + " and " + CLASS + ": individual characters"
            
            f.write(subtitle_temp.format(subtitle))
            s = visuals.colorize(strength, self.tokenized)
            f.write(s)
        


    def explain_with_filter(self, layer_name, filter_nb, arguments, dest_file, CLASS = None):

        keys = list(self.net_rel.keys())
        if not layer_name in keys:
            print("The layer name provided is not valid. Must be one of: ", keys)
            return
        
        title_temp = '<h1 style="color: black; text-align: center">{}</h1>'
        subtitle_temp = '<h2 style="color: black; text-align: center">{}</h2>'
        with open(os.path.join(dest_file, "filters_relevance.html"), 'w') as f:
            f.write(title_temp.format(self.title))
            
            if CLASS == None:
                rel = self.net_rel
                sub = self.label + "* : " + layer_name + " Filter " + str(filter_nb)
            else:
                rel = self.comp_rel[self.label + " > " + CLASS]
                sub = "Comp " + self.label + " and " + CLASS + ": " + layer_name + " Filter " + str(filter_nb)
        
            for g in range(rel[layer_name].shape[2]):
                subtitle = sub + " n-gram number " + str(g+1)
                try:
                    strength = rel[layer_name][0][filter_nb][g] * arguments.args[layer_name][0][filter_nb][g]
                except IndexError:
                    print("The filter number provided is invalid. Must be between 0 and ", len(self.net_rel[layer_name][0]) - 1)
                    return
                # strength = strength.detach().numpy()[:self.input_len]
                strength = strength.detach().numpy()
            
                if not np.array_equal(strength, np.full(strength.shape, 0.0)):
                    f.write(subtitle_temp.format(subtitle))
                    s = visuals.colorize(strength, self.tokenized)  
                    f.write(s)
    
    
    
    def explain_with_layer(self, layer_name, arguments, dest_file, CLASS = None):
        
        keys = list(self.net_rel.keys())
        if not layer_name in keys:
            print("The layer name provided is not valid. Must be one of: ", keys)
            return
        
        title_temp = '<h1 style="color: black; text-align: center">{}</h1>'
        subtitle_temp = '<h2 style="color: black; text-align: center">{}</h2>'
        
        with open(os.path.join(dest_file, "layer_relevance.html"), 'w') as f:
            f.write(title_temp.format(self.title))
            arg = torch.sum(arguments.args[layer_name], dim = 1, keepdim=True)
            
            if CLASS == None:
                rel = self.net_filters_collapsed[layer_name]
                sub = self.label + "* : " + layer_name
            else: 
                rel = self.comp_filters_collapsed[self.label + " > " + CLASS][layer_name]
                sub = "Comp " + self.label + " and " + CLASS + ": " + layer_name
        
            for g in range(rel.shape[2]):
                subtitle = sub + " n-gram number " + str(g+1)
                
                strength = rel[0][0][g] * arg[0][0][g]
                # strength = strength.detach().numpy()[:self.input_len]
                strength = strength.detach().numpy()
            
                if not np.array_equal(strength, np.full(strength.shape, 0.0)):
                    f.write(subtitle_temp.format(subtitle))
                    s = visuals.colorize(strength, self.tokenized)  
                    f.write(s) 
    
    
    
    def explain_with_biases(self, layer_name, CLASS = None):
 
        keys = list(self.net_rel.keys())
        if not layer_name in keys:
            print("The layer name provided is not valid. Must be one of: ", keys)
            return       
 
        if CLASS == None:
            rel = self.net_rel
            
        else:
            rel = self.comp_rel[self.label + " > " + CLASS]
            
        if layer_name == "Linear":
            strength = rel["Linear biases 1"].detach().numpy()
            title = layer_name + " biases 1"
            visuals.heatmap(strength, title)
            strength = rel["Linear biases 2"].detach().numpy()
            title = layer_name + " biases 2"
            visuals.heatmap(strength, title)
            strength = rel["Linear biases 3"].detach().numpy()
            title = layer_name + " biases 3"
            visuals.heatmap(strength, title)
        
        else:
            strength = rel[layer_name + " biases"].detach().numpy()
            title = layer_name + " biases"
            visuals.heatmap(strength, title)
        
        
        
    def explain_layer_with_chars(self, layer_name, arguments, dest_file, CLASS = None):
        
        arg = torch.sum(arguments.args[layer_name], dim = 1, keepdim=True)
        
        strength = torch.full(arg[0][0][0].shape, 0.0)
        
        if CLASS == None:
            rel = self.net_filters_collapsed[layer_name]
            sub = self.label + "* : " + layer_name + " characters collapsed"
        else: 
            rel = self.comp_filters_collapsed[self.label + " > " + CLASS][layer_name]
            sub = "Comp " + self.label + " and " + CLASS + ": " + layer_name + " characters collapsed"
        
        title_temp = '<h1 style="color: black; text-align: center">{}</h1>'
        subtitle_temp = '<h2 style="color: black; text-align: center">{}</h2>'
        
        with open(os.path.join(dest_file, "chars_at_layer.html"), 'w') as f:
            f.write(title_temp.format(self.title))
        
            for g in range(rel.shape[2]):
                strength += rel[0][0][g] * arg[0][0][g]
        
            # strength = strength.detach().numpy()[:self.input_len]
            strength = strength.detach().numpy()
              
            if not np.array_equal(strength, np.full(strength.shape, 0.0)):
                f.write(subtitle_temp.format(sub))
                s = visuals.colorize(strength, self.tokenized)  
                f.write(s)         
        
        
        
        
        