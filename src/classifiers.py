import torch
import torch.nn as nn
import torch.nn.functional as F


class ShallowCNN(nn.Module):
    ''' 
    Define network architecture and forward path. 
    Source: https://towardsdatascience.com/cnn-sentiment-analysis-9b1771e7cdd6 
    '''
    def __init__(self, vocab_size, output_dim, pad_idx, vector_size, n_filters, 
                 filter_sizes, dropout):
        
        super().__init__()
        # Create word embeddings from the input words     
        self.embedding = nn.Embedding(vocab_size, vector_size, 
                                      padding_idx = pad_idx)
        
        # Specify convolutions with filters of different sizes (fs)
        self.convs = nn.ModuleList([nn.Conv2d(in_channels = 1, 
                                              out_channels = n_filters, 
                                              kernel_size = (fs, vector_size)) 
                                    for fs in filter_sizes])
    
        # Add a fully connected layer for final predicitons
        self.linear = nn.Linear(len(filter_sizes) * n_filters, output_dim)
        
        # Drop some of the nodes to increase robustness in training
        self.dropout = nn.Dropout(dropout)
        
        
        
    def forward(self, text):
        '''Forward path of the network.'''
        # Get word embeddings and format them for convolutions
        mid = {}
        embedded = self.embedding(text).unsqueeze(1)
        mid["Embedded"] = embedded
        
        # Perform convolutions and apply activation functions
        conved = [F.relu(conv(embedded)).squeeze(3) for conv in self.convs]
        mid["Conved"] = conved
        
        # Pooling layer to reduce dimensionality WITH INDICES
        pooled_data = list()
        pooled_indices = list()
        for conv in conved: 
            # Cannot use nn.MaxPool1D (i.e. PyTorch's maxpooling layer) because 
            # need to know length of input first for stride size (because GLOBAL pooling)
            # data = F.max_pool1d(conv, conv.shape[2])
            data, index = F.max_pool1d(conv, conv.shape[2], return_indices=True)
            pooled_data.append(data.squeeze(2))
            pooled_indices.append(index.squeeze(2))
            
        mid["Pooled data"] = pooled_data
        mid["Pooled indices"] = pooled_indices
        
        cat_data = self.dropout(torch.cat(pooled_data, dim = 1))
        
        return self.linear(cat_data), mid
    
    

class DeeperCNN(nn.Module):
    ''' Define network architecture and forward path. '''
    def __init__(self, vocab_size, output_dim, pad_idx, vector_size, n_filters, 
                 filter_sizes, dropout):
        
        super().__init__()
        # Create word embeddings from the input words     
        self.embedding = nn.Embedding(vocab_size, vector_size, 
                                      padding_idx = pad_idx)
        
        # Specify convolutions with filters of different sizes (fs)
        self.convs = nn.ModuleList([nn.Conv2d(in_channels = 1, 
                                              out_channels = n_filters, 
                                              kernel_size = (fs, vector_size)) 
                                    for fs in filter_sizes])
        
        self.conv = nn.Conv1d(in_channels = 1,
                              out_channels = n_filters,
                              kernel_size = 2)
        
        # Add a fully connected layer for final predicitons
        self.linear = nn.Linear(n_filters, output_dim)
        
        # Drop some of the nodes to increase robustness in training
        self.dropout = nn.Dropout(dropout)
        
        
        
    def forward(self, text):
        '''Forward path of the network.'''       
        # Get word embeddings and format them for convolutions
        mid = {}
        embedded = self.embedding(text).unsqueeze(1)
        mid['Embedded'] = embedded
        
        # Perform convolutions and apply activation functions
        conved = [F.relu(conv(embedded)).squeeze(3) for conv in self.convs]
        mid['Conved'] = conved
            
        # Pooling layer to reduce dimensionality WITH INDICES
        pooled_data = list()
        pooled_indices = list()
        for conv in conved: 
            data, index = F.max_pool1d(conv, conv.shape[2], return_indices=True)
            pooled_data.append(data.squeeze(2))
            pooled_indices.append(index.squeeze(2))
        
        mid['Pooled data'] = pooled_data
        mid['Pooled indices'] = pooled_indices
        
        cat_data = self.dropout(torch.cat(pooled_data, dim = 1))      
        conved_2 = F.relu(self.conv(cat_data.unsqueeze(1)))
        mid['Conved 2'] = conved_2
        pooled_2, pooled_indices_2 = F.max_pool1d(conved_2, conved_2.shape[2], return_indices = True)
        mid['Pooled data 2'] = pooled_2
        mid['Pooled indices 2'] = pooled_indices_2
        
        return self.linear(pooled_2.squeeze(2)), mid
    


class RNN(nn.Module):
    def __init__(self, vocab_size, output_size, pad_idx, vector_size, hidden_size):
        super(RNN, self).__init__()
        
        self.embedding = nn.Embedding(vocab_size, vector_size, 
                                      padding_idx = pad_idx)
        self.hidden_size = hidden_size
        self.gru = nn.GRU(vector_size, hidden_size, batch_first=True)
        self.linear = nn.Linear(hidden_size, output_size)
        # self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, text, hidden):
        embedded = self.embedding(text)
        hidden_seq, hidden = self.gru(embedded, hidden)
        output = self.linear(hidden)
        # output = self.softmax(output)
        return output, hidden_seq, embedded

    def initHidden(self, batch_size = 1):
        return torch.zeros(1, batch_size, self.hidden_size)
    
    
  
    
class VDCNN_17(nn.Module):
    ''' 
    Very Deep CNN as described in https://arxiv.org/abs/1606.01781
    with 17 (non-residual) convolutional layers
    Source: https://github.com/threelittlemonkeys/vdcnn-pytorch
    '''
    def __init__(self, vocab_size, output_dim, vector_size, kernel_size, stride, padding):
        
        super().__init__()
        self.k = 8 # k-max pooling
        self.vocab_size = vocab_size

        # architecture
        self.embed = nn.Embedding(vocab_size, vector_size)
        self.conv = nn.Conv1d(vector_size, 64, kernel_size, stride, padding)
        self.res_blocks = nn.Sequential( # residual blocks
            ResBlock(64, 64, kernel_size, stride, padding),
            ResBlock(64, 64, kernel_size, stride, padding, True),
            ResBlock(64, 128, kernel_size, stride, padding),
            ResBlock(128, 128, kernel_size, stride, padding, True),
            ResBlock(128, 256, kernel_size, stride, padding),
            ResBlock(256, 256, kernel_size, stride, padding, True),
            ResBlock(256, 512, kernel_size, stride, padding),
            ResBlock(512, 512, kernel_size, stride, padding)
        )
        self.fc = nn.Sequential( # fully connected layers
            nn.Linear(512 * self.k, 2048),
            nn.ReLU(),
            nn.Linear(2048, 2048),
            nn.ReLU(),
            nn.Linear(2048, output_dim)
        )

        # if pp.device == 'cuda':
        #     self = self.cuda()
            

    def forward(self, x):
        mid = {}
        
        x = self.embed(x) # embedding
        mid["Embedded"] = x
        
        x = x.transpose(1, 2) # [batch_size (N), num_feature_maps (D), seq_len (L)]
        mid["Transposed"] = x

        h = self.conv(x) # temporal convolution
        mid["Conved"] = h
        
        h = self.res_blocks(h) # residual blocks
        mid["Resed"] = h

        dim = h.shape[0]
        top_k = h.topk(self.k)[0].view(dim, -1) # k-max pooling
        mid["Top-K"] = top_k
        mid["Top-K indices"] = h.topk(self.k)[1]

        y = self.fc(top_k) # fully connected layers
        mid["Output"] = y

        return y, mid



class VDCNN_29(nn.Module):
    ''' 
    Very Deep CNN as described in https://arxiv.org/abs/1606.01781
    with 17 (non-residual) convolutional layers
    Source: https://github.com/threelittlemonkeys/vdcnn-pytorch
    '''    
    def __init__(self, vocab_size, output_dim, vector_size, kernel_size, stride, padding):
        
        super().__init__()
        self.k = 8 # k-max pooling
        self.vocab_size = vocab_size

        # architecture
        self.embed = nn.Embedding(vocab_size, vector_size)
        self.conv = nn.Conv1d(vector_size, 64, kernel_size, stride, padding)
        self.res_blocks = nn.Sequential( # residual blocks
            ResBlock(64, 64, kernel_size, stride, padding),
            ResBlock(64, 64, kernel_size, stride, padding),
            ResBlock(64, 64, kernel_size, stride, padding),
            ResBlock(64, 64, kernel_size, stride, padding),
            ResBlock(64, 64, kernel_size, stride, padding, True),
            ResBlock(64, 128, kernel_size, stride, padding),
            ResBlock(128, 128, kernel_size, stride, padding),
            ResBlock(128, 128, kernel_size, stride, padding),
            ResBlock(128, 128, kernel_size, stride, padding),
            ResBlock(128, 128, kernel_size, stride, padding, True),
            ResBlock(128, 256, kernel_size, stride, padding),
            ResBlock(256, 256, kernel_size, stride, padding, True),
            ResBlock(256, 512, kernel_size, stride, padding),
            ResBlock(512, 512, kernel_size, stride, padding)
        )
        self.fc = nn.Sequential( # fully connected layers
            nn.Linear(512 * self.k, 2048),
            nn.ReLU(),
            nn.Linear(2048, 2048),
            nn.ReLU(),
            nn.Linear(2048, output_dim)
        )

            

    def forward(self, x):
        mid = {}
        
        x = self.embed(x) # embedding
        mid["Embedded"] = x
        
        x = x.transpose(1, 2) # [batch_size (N), num_feature_maps (D), seq_len (L)]
        mid["Transposed"] = x
        
        h = self.conv(x) # temporal convolution
        mid["Conved"] = h
        
        h = self.res_blocks(h) # residual blocks
        mid["Resed"] = h
        
        dim = h.shape[0]
        top_k = h.topk(self.k)[0].view(dim, -1) # k-max pooling
        mid["Top-K"] = top_k
        mid["Top-K indices"] = h.topk(self.k)[1]
        
        y = self.fc(top_k) # fully connected layers
        mid["Output"] = y
        
        return y, mid





class ResBlock(nn.Module): # residual block
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding, downsample = False):
        super().__init__()
        first_stride = 1
        pool_stride = 2 if downsample else 1

        # architecture
        self.conv_block = ConvBlock(in_channels, out_channels, first_stride, kernel_size, stride, padding)
        self.pool = None
        
        if downsample: # VGG-like
            self.pool = nn.MaxPool1d(kernel_size, pool_stride, padding, return_indices=True)
        self.shortcut = nn.Conv1d(in_channels, out_channels, 1, pool_stride)


    def forward(self, x):
        
        self.mid = {}
        self.mid["Input"] = x
        
        conved = self.conv_block(x)
        self.mid["Conved"] = conved
        y = conved
        
        if self.pool:
            pooled, indices = self.pool(conved)
            self.mid["Pooled"] = pooled
            self.mid["Pooled indices"] = indices
            y = pooled
            
        shortcut = self.shortcut(x) # ResNet shortcut connections
        self.mid["Shortcut"] = shortcut
        output = y + shortcut # ResNet shortcut connections
        self.mid["Output"] = output
        
        return output



class ConvBlock(nn.Module): # convolutional block
    def __init__(self, in_channels, out_channels, first_stride, kernel_size, stride, padding):
        super().__init__()
        # architecture
        self.sequential = nn.Sequential(
            nn.Conv1d(in_channels, out_channels, kernel_size, first_stride, padding),
            nn.BatchNorm1d(out_channels),
            nn.ReLU(),
            nn.Conv1d(out_channels, out_channels, kernel_size, stride, padding),
            nn.BatchNorm1d(out_channels),
            nn.ReLU()
        )

    def forward(self, x):
        return self.sequential(x)
    
    
    