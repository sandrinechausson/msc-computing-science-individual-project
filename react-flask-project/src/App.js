import React, { useState, useEffect } from 'react';
import { Input, Grid, Button, List, Icon, Divider, Popup } from 'semantic-ui-react';
import parse, { domToReact } from 'html-react-parser';
import './App.css';

function App() {

  const [title, setTitle] = useState("");
  const [article, setArticle] = useState("")
  const [prediction, setPrediction] = useState();
  const [loading, setLoading] = useState(false)
  const [randomLoading, setRandomLoading] = useState(false)

  const [trueLabel, setTrueLabel] = useState('')

  const [whyNotVisible, setWhyNotVisible] = useState(false);
  const [whyVisible, setWhyVisible] = useState(false)

  const [altClasses, setAltClasses] = useState();

  const [moreArgDisabled, setMoreArgDisabled] = useState(false);
  const [lessArgDisabled, setLessArgDisabled] = useState(true);
  const [moreAlt1Disabled, setMoreAlt1Disabled] = useState(false);
  const [lessAlt1Disabled, setLessAlt1Disabled] = useState(true);
  const [moreAlt2Disabled, setMoreAlt2Disabled] = useState(false);
  const [lessAlt2Disabled, setLessAlt2Disabled] = useState(true);
  const [moreAlt3Disabled, setMoreAlt3Disabled] = useState(false);
  const [lessAlt3Disabled, setLessAlt3Disabled] = useState(true);

  const [alt1Visible, setAlt1Visible] = useState(false)
  const [alt2Visible, setAlt2Visible] = useState(false)
  const [alt3Visible, setAlt3Visible] = useState(false)

  const [argCounter, setArgCounter] = useState();
  const [argList, setArgList] = useState([]);
  const [support, setSupport] = useState([]);

  const [alt1Counter, setAlt1Counter] = useState();
  const [alt1List, setAlt1List] = useState([]);
  const [alt1, setAlt1] = useState([]);

  const [alt2Counter, setAlt2Counter] = useState();
  const [alt2List, setAlt2List] = useState([]);
  const [alt2, setAlt2] = useState([]);

  const [alt3Counter, setAlt3Counter] = useState();
  const [alt3List, setAlt3List] = useState([]);
  const [alt3, setAlt3] = useState([]);

  const [basicExplanation, setBasicExplanation] = useState('')

  const [wordsVisibility, setWordsVisibility] = useState()
  const [filtersVisibility, setFiltersVisibility] = useState()
  const [filtersCounter, setFiltersCounter] = useState()

  const [wordIDCounter, setWordIDCounter] = useState(0)

  const [type2List, setType2List] = useState()
  const [type2Explanation, setType2Explanation] = useState()
  const [type2Visible, setType2Visible] = useState(-1)
  const [type2FilterCounter, setType2FilterCounter] = useState()

  const [type2WordsVisible, setType2WordsVisible] = useState({})
  const [type2FiltersVisible, setType2FiltersVisible] = useState({})


  const getRandomArticle = async () => {
    const response = await fetch('/api/get_random_article', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({"title": title})
    });

    if (response.ok) {
      console.log('Random article response ok')
      response.json().then((data => {
        setTitle(data.title)
        setTrueLabel(data.true_label)
      }))
      setRandomLoading(false)
    }
  }


  const handleSubmit = async () => {
    const response = await fetch('/api/prediction', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({"title": title})
    });

    if (response.ok) {
      response.json().then((data) => {

        setMoreArgDisabled(false);
        setLessArgDisabled(true);
        setMoreAlt1Disabled(false);
        setLessAlt1Disabled(true);
        setMoreAlt2Disabled(false);
        setLessAlt2Disabled(true);
        setMoreAlt3Disabled(false);
        setLessAlt3Disabled(true);

        setAlt1Visible(false)
        setAlt2Visible(false)
        setAlt3Visible(false)
        setWordsVisibility({'Arg': [false], 'Alt1': [false], 'Alt2': [false], 'Alt3': [false]})
        setFiltersVisibility({'Arg': [false], 'Alt1': [false], 'Alt2': [false], 'Alt3': [false]})

        setArgCounter(1)
        setAlt1Counter(1)
        setAlt2Counter(1)
        setAlt3Counter(1)
        setFiltersCounter({'Arg': [1], 'Alt1': [1], 'Alt2': [1], 'Alt3': [1]})

        setWhyNotVisible(false)
        setWhyVisible(false)
        setAltClasses(data.alt_classes)
        setPrediction(data.prediction)

        setArgList(data.arguments)
        setSupport([{...data.arguments[0], ['ID']: 0}])

        setAlt1List(data.alt_1)
        setAlt1([{...data.alt_1[0], ['ID']: 100}])

        setAlt2List(data.alt_2)
        setAlt2([{...data.alt_2[0], ['ID']: 200}])

        setAlt3List(data.alt_3)
        setAlt3([{...data.alt_3[0], ['ID']: 300}])

        setType2List(data.type2_explanation)
        setType2Explanation()
        setType2Visible(-1)

        let nb_words = data.type2_explanation.length
        var visible_w = {}
        var visible_f = {}
        var counter = {}
        data.type2_explanation.map((item, key) => {
          visible_w = {...visible_w, [key.toString()]: Array(item.length).fill(false)}
          visible_f = {...visible_f, [key.toString()]: Array(item.length).fill(false)}
          counter = {...counter, [key.toString()]: Array(item.length).fill(1)}
        })
        setType2WordsVisible(visible_w)
        setType2FiltersVisible(visible_f)
        setType2FilterCounter(counter)

        setBasicExplanation(data.basic_explanation)

        setArticle(title)
        setLoading(false)
      })
      setTitle('')
    } else {
      console.log("responses didnt work")
    }
  }


  const addArgument = () => {
    if (support.length == (argList.length-1)) {
      setMoreArgDisabled(true)
    }
    if (support.length >= 1) {
      setLessArgDisabled(false)
    }
    setSupport([...support, {...argList[argCounter], ['ID']: argCounter}])
    setWordsVisibility({...wordsVisibility, ['Arg']: [...wordsVisibility['Arg'], false]})
    setFiltersVisibility({...filtersVisibility, ['Arg']: [...filtersVisibility['Arg'], false]})
    setFiltersCounter({...filtersCounter, ['Arg']: [...filtersCounter['Arg'], 1]})
    setArgCounter(argCounter+1)
  }


  const removeArgument = () => {
    if (support.length == 2) {
      setLessArgDisabled(true)
    }
    setSupport(support.splice(0,(support.length - 1)))
    setArgCounter(argCounter - 1)
    setMoreArgDisabled(false)
  }

  const renderWords = (arg) => {
    var parse = require('html-react-parser')
    if (arg.ID >= 300) {
      var cat = 'Alt3'
      var index = arg.ID - 300
    } else if (arg.ID >= 200) {
      var cat = 'Alt2'
      var index = arg.ID - 200
    } else if (arg.ID >= 100) {
      var cat = 'Alt1'
      var index = arg.ID - 100
    } else {
      var cat = 'Arg'
      var index = arg.ID
    }
    if (wordsVisibility[cat][index]) {
      return (
        <div className = "WordsExplanation">
          {parse(arg.Words)}
        </div>
      )
    }
  }


  const renderType2Words = (arg, index, key) => {
    var parse = require('html-react-parser')
    if (type2WordsVisible[index.toString()][key]) {
      return (
        <div className = "WordsExplanation">
          {parse(arg.Words)}
        </div>
      )
    }
  }


  const addFilter = (cat, index) => {
    var new_counter = filtersCounter[cat]
    new_counter[index] = new_counter[index] + 1
    setFiltersCounter({...filtersCounter, [cat]: new_counter})
  }


  const removeFilter = (cat, index) => {
    var new_counter = filtersCounter[cat]
    new_counter[index] = new_counter[index] - 1
    setFiltersCounter({...filtersCounter, [cat]: new_counter})
  }


  const renderFilters = (arg) => {
    var parse = require('html-react-parser')
    if (arg.ID >= 300) {
      var cat = 'Alt3'
      var index = arg.ID - 300
      var full_list = argList
    } else if (arg.ID >= 200) {
      var cat = 'Alt2'
      var index = arg.ID - 200
      var full_list = alt1List
    } else if (arg.ID >= 100) {
      var cat = 'Alt1'
      var index = arg.ID - 100
      var full_list = alt2List
    } else {
      var cat = 'Arg'
      var index = arg.ID
      var full_list = alt3List
    }

    if (filtersVisibility[cat][index]) {
      var filters_list = arg.Filters.slice(0,filtersCounter[cat][index])
      return (
        <div className = "FiltersBlock">
          <div className = "Filters">
            <Grid columns = {2}>
              {filters_list.map((item) => <Grid.Column width={8}>{parse(item)}</Grid.Column>)}
            </Grid>
          </div>
          <div className="FiltersMoreLessButtonsGroup">
            <Button.Group floated='right'>
              <Button
                onClick = {() => addFilter(cat, index)}
                disabled = {filtersCounter[cat][index] == full_list[index]['Filters'].length}
              ><Icon name="plus"/></Button>
              <Button
                onClick = {() => removeFilter(cat, index)}
                disabled = {filtersCounter[cat][index] == 1}
              >
                <Icon name="minus"/>
              </Button>
            </Button.Group>
          </div>
        </div>
      )
    }
  }


  const renderType2Filters = (arg, index, key) => {
    var parse = require('html-react-parser')
    console.log("arg.Filters.length: ", arg.Filters.length)
    if (type2FiltersVisible[index.toString()][key]) {
      var filters_list = arg.Filters.slice(0,type2FilterCounter[index.toString()][key])
      return (
        <div className = "FiltersBlock">
          <div className = "Filters">
            <Grid columns = {2}>
              {filters_list.map((item) => <Grid.Column width={8}>{parse(item)}</Grid.Column>)}
            </Grid>
          </div>
          <div className="FiltersMoreLessButtonsGroup">
            <Button.Group floated='right'>
              <Button
                onClick = {() => {
                  var new_counter = type2FilterCounter[index.toString()]
                  new_counter[key] = new_counter[key] + 1
                  setType2FilterCounter({...type2FilterCounter, [index.toString()]: new_counter})
                }}
                disabled = {type2FilterCounter[index.toString()][key] == arg.Filters.length}
              ><Icon name="plus"/></Button>
              <Button
                onClick = {() => {
                  var new_counter = type2FilterCounter[index.toString()]
                  new_counter[key] = new_counter[key] - 1
                  setType2FilterCounter({...type2FilterCounter, [index.toString()]: new_counter})
                }}
                disabled = {type2FilterCounter[index.toString()][key] == 1}
              >
                <Icon name="minus"/>
              </Button>
            </Button.Group>
          </div>
        </div>
      )
    }
  }


  const showWords = (arg) => {
    if (arg.ID >= 300) {
      var cat = 'Alt3'
      var index = arg.ID - 300
    } else if (arg.ID >= 200) {
      var cat = 'Alt2'
      var index = arg.ID - 200
    } else if (arg.ID >= 100) {
      var cat = 'Alt1'
      var index = arg.ID - 100
    } else {
      var cat = 'Arg'
      var index = arg.ID
    }
    var new_word_visibility = wordsVisibility[cat]
    new_word_visibility[index] = !(new_word_visibility[index])
    setWordsVisibility({...wordsVisibility, [cat]: new_word_visibility})

    var new_filter_visibility = filtersVisibility[cat]
    new_filter_visibility[index] = false
    setFiltersVisibility({...filtersVisibility, [cat]: new_filter_visibility})
  }


  const showFilters = (arg) => {
    if (arg.ID >= 300) {
      var cat = 'Alt3'
      var index = arg.ID - 300
    } else if (arg.ID >= 200) {
      var cat = 'Alt2'
      var index = arg.ID - 200
    } else if (arg.ID >= 100) {
      var cat = 'Alt1'
      var index = arg.ID - 100
    } else {
      var cat = 'Arg'
      var index = arg.ID
    }

    var new_filter_visibility = filtersVisibility[cat]
    new_filter_visibility[index] = !(new_filter_visibility[index])
    setFiltersVisibility({...filtersVisibility, [cat]: new_filter_visibility})

    var new_word_visibility = wordsVisibility[cat]
    new_word_visibility[index] = false
    setWordsVisibility({...wordsVisibility, [cat]: new_word_visibility})
  }


  const renderArg = (arg) => {

    if (arg.ID >= 300) {
      var cat = 'Alt3'
      var index = arg.ID - 300
    } else if (arg.ID >= 200) {
      var cat = 'Alt2'
      var index = arg.ID - 200
    } else if (arg.ID >= 100) {
      var cat = 'Alt1'
      var index = arg.ID - 100
    } else {
      var cat = 'Arg'
      var index = arg.ID
    }

    var parse = require('html-react-parser')
    return (
      <div className="Argument">
        <div className = "Explanation">
          {parse(arg.Html)}
        </div>
        {renderWords(arg)}
        {renderFilters(arg)}
        <Button.Group className = "GoDeeperButtonGroup" attached='bottom'>
          <Button
            onClick={() => showWords(arg)}
            active={wordsVisibility[cat][index]}
          >See words
            <Popup
              content={"This option allows you to decompose the contribution of this group of words into its individual words."}
              trigger={<div className='InfoIcon'><Icon name='question circle' size='small'/></div>} />
          </Button>
          <Button
            onClick={() => showFilters(arg)}
            active={filtersVisibility[cat][index]}
          >See filters
            <Popup
              content={'This option allows you to see which filters in the model mostly detected this group of words, and the concepts originally detected by these filters in the training set.'}
              trigger={<div className='InfoIcon'><Icon name='question circle' size='small'/></div>} />
          </Button>
        </Button.Group>
      </div>
    )
  }


  const showType2Words = (index, ngram) => {
    let words_copy = type2WordsVisible[index.toString()]
    words_copy[ngram] = !(words_copy[ngram])
    setType2WordsVisible({...type2WordsVisible, [index.toString()]: words_copy})

    let filters_copy = type2FiltersVisible[index.toString()]
    filters_copy[ngram] = false
    setType2FiltersVisible({...type2FiltersVisible, [index.toString()]: filters_copy})
  }

  const showType2Filters = (index, ngram) => {
    let filters_copy = type2FiltersVisible[index.toString()]
    filters_copy[ngram] = !(filters_copy[ngram])
    setType2FiltersVisible({...type2FiltersVisible, [index.toString()]: filters_copy})

    let words_copy = type2WordsVisible[index.toString()]
    words_copy[ngram] = false
    setType2WordsVisible({...type2WordsVisible, [index.toString()]: words_copy})
  }



  const renderType2Arg = (arg, key) => {

    var parse = require('html-react-parser')
    return (
      <Grid.Column>
        <div className="Argument">
          <div className = "Explanation">
            {parse(arg.Html)}
          </div>
          {renderType2Words(arg, type2Visible, key)}
          {renderType2Filters(arg, type2Visible, key)}
          <Button.Group className = "GoDeeperButtonGroup" attached='bottom'>
            <Button
              onClick={() => showType2Words(type2Visible, key)}
              active={type2WordsVisible[type2Visible.toString()][key]}
            >See words
              <Popup
                content={"This option allows you to decompose the contribution of this group of words into its individual words."}
                trigger={<div className='InfoIcon'><Icon name='question circle' size='small'/></div>} />
            </Button>
            <Button
              onClick={() => showType2Filters(type2Visible, key)}
              active={type2FiltersVisible[type2Visible.toString()][key]}
            >See filters
              <Popup
                content={'This option allows you to see which filters in the model mostly detected this group of words, and the concepts originally detected by these filters in the training set.'}
                trigger={<div className='InfoIcon'><Icon name='question circle' size='small'/></div>} />
            </Button>
          </Button.Group>
        </div>
      </Grid.Column>
    )
  }


  const renderSupport = () => {
    if (whyVisible) {
      return (
        <div className = 'ArgumentsList'>
          <div className = "Arguments">
            {support.map((item) => renderArg(item))}
          </div>
          <div className="MoreLessButtonGroup">
            <Button onClick={() => addArgument()} disabled={moreArgDisabled}>More reasons</Button>
            <Button onClick={() => removeArgument()} disabled={lessArgDisabled}>Less reasons</Button>
          </div>
        </div>
      )
    }
  }


  const addAlt1Arg = () => {
    if (alt1.length == (alt1List.length-1)) {
      setMoreAlt1Disabled(true)
    }
    if (alt1.length >= 1) {
      setLessAlt1Disabled(false)
    }
    setAlt1([...alt1, {...alt1List[alt1Counter], ['ID']: 100+alt1Counter}])
    setWordsVisibility({...wordsVisibility, ['Alt1']: [...wordsVisibility['Alt1'], false]})
    setFiltersVisibility({...filtersVisibility, ['Alt1']: [...filtersVisibility['Alt1'], false]})
    setFiltersCounter({...filtersCounter, ['Alt1']: [...filtersCounter['Alt1'], 1]})
    setAlt1Counter(alt1Counter+1)
  }

  const removeAlt1Arg = () => {
    if (alt1.length == 2) {
      setLessAlt1Disabled(true)
    }
    setAlt1(alt1.splice(0,(alt1.length - 1)))
    setAlt1Counter(alt1Counter - 1)
    setMoreAlt1Disabled(false)
  }


  const addAlt2Arg = () => {
    if (alt2.length == (alt2List.length-1)) {
      setMoreAlt2Disabled(true)
    }
    if (alt2.length >= 1) {
      setLessAlt2Disabled(false)
    }
    setAlt2([...alt2, {...alt2List[alt2Counter], ['ID']: 200+alt2Counter}])
    setWordsVisibility({...wordsVisibility, ['Alt2']: [...wordsVisibility['Alt2'], false]})
    setFiltersVisibility({...filtersVisibility, ['Alt2']: [...filtersVisibility['Alt2'], false]})
    setFiltersCounter({...filtersCounter, ['Alt2']: [...filtersCounter['Alt2'], 1]})
    setAlt2Counter(alt2Counter+1)
  }

  const removeAlt2Arg = () => {
    if (alt2.length == 2) {
      setLessAlt2Disabled(true)
    }
    setAlt2(alt2.splice(0,(alt2.length - 1)))
    setAlt2Counter(alt2Counter - 1)
    setMoreAlt2Disabled(false)
  }


  const addAlt3Arg = () => {
    if (alt3.length == (alt3List.length-1)) {
      setMoreAlt3Disabled(true)
    }
    if (alt3.length >= 1) {
      setLessAlt3Disabled(false)
    }
    setAlt3([...alt3, {...alt3List[alt3Counter], ['ID']: 300+alt3Counter}])
    setWordsVisibility({...wordsVisibility, ['Alt3']: [...wordsVisibility['Alt3'], false]})
    setFiltersVisibility({...filtersVisibility, ['Alt3']: [...filtersVisibility['Alt3'], false]})
    setFiltersCounter({...filtersCounter, ['Alt3']: [...filtersCounter['Alt3'], 1]})
    setAlt3Counter(alt3Counter+1)
  }


  const removeAlt3Arg = () => {
    if (alt3.length == 2) {
      setLessAlt3Disabled(true)
    }
    setAlt3(alt3.splice(0,(alt3.length - 1)))
    setAlt3Counter(alt3Counter - 1)
    setMoreAlt3Disabled(false)
  }


  const renderAlt = () => {
    if (alt1Visible) {
      return (
        <div className = 'ArgumentsList'>
          <div className = "Arguments">
            {alt1.map((item) => renderArg(item))}
          </div>
          <div className="MoreLessButtonGroup">
            <Button onClick={() => addAlt1Arg()} disabled={moreAlt1Disabled}>Tell me more</Button>
            <Button onClick={() => removeAlt1Arg()} disabled={lessAlt1Disabled}>Tell me less</Button>
          </div>
        </div>
      )
    };
    if (alt2Visible) {
      return (
        <div className = 'ArgumentsList'>
          <div className = "Arguments">
            {alt2.map((item) => renderArg(item))}
          </div>
          <div className="MoreLessButtonGroup">
            <Button onClick={() => addAlt2Arg()} disabled={moreAlt2Disabled}>Tell me more</Button>
            <Button onClick={() => removeAlt2Arg()} disabled={lessAlt2Disabled}>Tell me less</Button>
          </div>
        </div>
      )
    };
    if (alt3Visible) {
      return (
        <div className = 'ArgumentsList'>
          <div className = "Arguments">
            {alt3.map((item) => renderArg(item))}
          </div>
          <div className="MoreLessButtonGroup">
            <Button onClick={() => addAlt3Arg()} disabled={moreAlt3Disabled}>Tell me more</Button>
            <Button onClick={() => removeAlt3Arg()} disabled={lessAlt3Disabled}>Tell me less</Button>
          </div>
        </div>
      )
    };
  }

  const clickAlt1 = () => {
    setAlt1Visible(!alt1Visible)
    setAlt2Visible(false)
    setAlt3Visible(false)
  }

  const clickAlt2 = () => {
    setAlt1Visible(false)
    setAlt2Visible(!alt2Visible)
    setAlt3Visible(false)
  }


  const clickAlt3 = () => {
    setAlt1Visible(false)
    setAlt2Visible(false)
    setAlt3Visible(!alt3Visible)
  }

  const renderContrastive = () => {
    if (whyNotVisible) {
      return (
        <div>
          <div className = "WhyWhyNot">
            <Button.Group>
              <Button onClick={() => clickAlt1()} active={alt1Visible}>{altClasses[0]}</Button>
              <Button onClick={() => clickAlt2()} active={alt2Visible}>{altClasses[1]}</Button>
              <Button onClick={() => clickAlt3()} active={alt3Visible}>{altClasses[2]}</Button>
            </Button.Group>
          </div>
          {renderAlt()}
        </div>
      )
    }
  }

  const clickable = {
    replace: ({ attribs, children }) => {
      var index = children[0].attribs.id
      if (attribs.class === 'clickable') {
        return (
          <div className="WordButton">
            <Button
              basic
              onClick={() => {
                if (type2Visible == index) {
                  setType2Visible(-1)
                  setType2Explanation()
                } else {
                  setType2Visible(index)
                  setType2Explanation(type2List[index])
                }
              }}
              active={type2Visible == index}
              disabled = {type2List[index.toString()].length == 0}
            >{domToReact(children)}</Button>
          </div>
        )
      }
    }
  }


  const renderType2Explanation = () => {
    if (type2Visible != -1) {
      return (
        <div className = "Arguments">
          <Grid columns={3}>
            <Grid.Row>
              {type2Explanation.map((item, key) => renderType2Arg(item, key))}
            </Grid.Row>
          </Grid>
        </div>
      )
    }
  }

  const renderPrediction = () => {
    var parse = require('html-react-parser')
    if (prediction && !loading) {
      var message = 'Other classes are: ' + altClasses[0] + ', ' + altClasses[1] + ', ' + altClasses[2]
      if (trueLabel != '') {
        message = message + '. The true label for this article is actually ' + trueLabel + '.'
      }
      return (
        <div className = "PageBody">
          <div className="ArticleDisplay">
            <p className="Article">{article}</p>
          </div>
          <div className="Prediction">
            <p>I believe this article belongs to the class: <b>{prediction}</b>
              <Popup
                content={message}
                trigger={<div className='InfoIcon'><Icon floated='top' name='question circle' size="small"/></div>} /></p>
          </div>
          <Divider horizontal>Explanation type 1</Divider>
          <div className="ExplanationType">
          <Grid columns={2}>
            <Grid.Column>
              <div className = "WhyWhyNot">
                <Button
                  onClick = {() => setWhyVisible(!whyVisible)
                }>Why?
                  <Popup
                    content={'This option allows you to see ngrams (i.e. groups of words) present in the article that contributed to the prediction, in descending order.'}
                    trigger={<div className='InfoIcon'><Icon floated='top' name='question circle' size='small'/></div>} />
                </Button>
              </div>
              {renderSupport()}
            </Grid.Column>
            <Grid.Column>
              <div className = "WhyWhyNot">
                <Button
                  className = "WhyWhyNot"
                  onClick = {() => setWhyNotVisible(!whyNotVisible)
                }>Why not...
                <Popup
                  content={'This option allows you to see groups of words present in the article that contributed the most against some alternative class.'}
                  trigger={<div className='InfoIcon'><Icon floated='top' name='question circle' size='small'/></div>} />
                </Button>
              </div>
              {renderContrastive()}
            </Grid.Column>
          </Grid>
          </div>
          <Divider horizontal>Explanation type 2</Divider>
          <div className="ExplanationType">
            <div className = "BasicExplanation">
              {parse(basicExplanation, clickable)}
              <Popup
                content={'This diagram shows the words that mostly contributed in favour (in green) and against (in red) the prediction. By clicking on a word, you can see the contribution of different groups the word belongs to. Disabled words are those which do not belong to any group of words that contributed to the output.'}
                trigger={<div className='InfoIcon'><Icon name='question circle' size='small'/></div>} />
            </div>
            {renderType2Explanation()}
          </div>
        </div>
      )
    } else {
      return (
        <div className="WelcomeMessage">
          <h2>Paste an article in the input box above or click on "Get random article" and submit to see my prediction with an explanation!</h2>
        </div>
      )
    }
  }


  return (
    <div className="App">
      <header className="Input">
        <div className = "PageTitle">
          <h1>Input article's title:</h1>
        </div>
        <Input
          placeholder = "News article title"
          type="text"
          value={title}
          onChange={e => setTitle(e.target.value)}
          className = "UserInput"
        />
        <div className = "HeaderButtons">
          <Button
            onClick = {() => {
              setRandomLoading(true)
              getRandomArticle()
            }}
            loading = {randomLoading}
            >Get random article</Button>
          <Button
            onClick = {() => {
              setLoading(true)
              handleSubmit()
            }}
            disabled = {title == ''}
            color = 'blue'
            loading = {(loading)}
          >Submit</Button>
        </div>
      </header>
      {renderPrediction()}
    </div>
  );
}

export default App;
