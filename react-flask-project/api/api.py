from flask import Flask, jsonify, request
import random
import csv
import json

import pickle
import os
import sys

import torch
import spacy

REPO_PATH = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
print(REPO_PATH)
sys.path.append(os.path.join(REPO_PATH, "src"))

import classifiers
import explainers
# import preprocessing as pp
import filter_concepts_extraction as fce


def define_model():

    INPUT_DIM = len(VOCAB)
    EMBEDDING_DIM = 100
    N_FILTERS = 50
    FILTER_SIZES = [2, 3, 4]
    OUTPUT_DIM = 4
    DROPOUT = 0.5
    PAD_IDX = VOCAB.stoi[PAD_TOK]

    # Initialize model and load pre-trained embeddings
    model = classifiers.ShallowCNN(INPUT_DIM, OUTPUT_DIM, PAD_IDX, EMBEDDING_DIM,
                                    N_FILTERS, FILTER_SIZES, DROPOUT)

    model.load_state_dict(torch.load(os.path.join(NEWS_DIR, "model.pt"), map_location=DEVICE))

    model.eval()

    return model


def get_concepts():

    if not os.path.isfile(NEWS_DIR + "/top_K_sentences.pickle"):
        fce.top_k_sentences(MODEL, NEWS_DIR + "/top_K_sentences.pickle", TOKENIZER, VOCAB)

    top_K = pickle.load(open(NEWS_DIR + "/top_K_sentences.pickle", "rb"))
    concepts = fce.get_concepts_from_topK_sentences(top_K, TOKENIZER)
    sentences = fce.create_synth_sentences(concepts)
    top_M = fce.top_M_concepts(MODEL, sentences, TOKENIZER, VOCAB)

    return top_M


nlp = spacy.load('en')
DEVICE = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
PAD_TOK = "<pad>"
UNK_TOK = "<unk>"

NEWS_DIR = os.path.join(REPO_PATH, "outputs", "News_Description_ShallowCNN")
TOKENIZER = nlp.tokenizer
VOCAB = pickle.load(open(NEWS_DIR + "/field_vocab.pickle", "rb"))
LABELS = pickle.load(open(NEWS_DIR + "/label_vocab.pickle", "rb"))
CLASSES = ["World", "Sports", "Business", "Sci/Tech"]
MODEL = define_model()
MODEL.load_state_dict(torch.load(NEWS_DIR + '/model.pt', map_location=DEVICE))
CONCEPTS = get_concepts()
EXPLAINER = None
COUNTER = 0


# app = Flask(__name__)
app = Flask(__name__, static_folder='../build', static_url_path='/')

@app.route('/')
def index():
    return app.send_static_file('index.html')

@app.route("/api/prediction", methods = ['POST'])
def prediction():
    data = request.get_json()
    global EXPLAINER
    EXPLAINER = explainers.Explainer_CNN(MODEL, data['title'], TOKENIZER, VOCAB, LABELS, DEVICE)
    prediction = EXPLAINER.label
    alt_classes = []
    for i in range(4):
        if CLASSES[i] != prediction:
            alt_classes.append(CLASSES[i])

    type2_explanation = []
    for i in range(EXPLAINER.input_size):
        type2_explanation.append(EXPLAINER.get_ngrams_from_words(i, CONCEPTS))
        print("i = ", i, " okay")

    result = {
        'prediction': prediction,
        'alt_classes': alt_classes,
        'arguments': EXPLAINER.top_ngrams(CONCEPTS),
        'alt_1': EXPLAINER.top_ngrams(CONCEPTS, alt_classes[0]),
        'alt_2': EXPLAINER.top_ngrams(CONCEPTS, alt_classes[1]),
        'alt_3': EXPLAINER.top_ngrams(CONCEPTS, alt_classes[2]),
        'basic_explanation': EXPLAINER.explain_with_words(render='html'),
        'type2_explanation' : type2_explanation,
    }
    oz = None
    
    with open('oz.json', 'r') as f:
        oz = json.load(f)
        oz[data['title']] = result

    with open('oz.json', 'w') as f:
        json.dump(oz, f, indent=4)
        global COUNTER
        print("Saved to json! ", COUNTER)
        COUNTER += 1

    return jsonify(result)

@app.route("/api/get_random_article", methods = ['POST'])
def get_random_article():
    n = random.randint(0, 7599)

    with open(REPO_PATH+"/news_data/test.csv") as fd:
        reader=csv.reader(fd)
        for idx, row in enumerate(reader):
            if idx == n:
                this_row = row

    true_label = CLASSES[int(this_row[0]) - 1]
    title = this_row[1]
    descr = this_row[2]

    return jsonify({'title': descr, 'true_label': true_label})

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=False, port=os.environ.get('PORT', 80))
