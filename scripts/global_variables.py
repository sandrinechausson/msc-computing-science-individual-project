import os.path
import torch
import spacy

# Load english language
nlp = spacy.load('en')
REPO_PATH = os.path.dirname(os.path.dirname(__file__))
PAD_TOK =  "<pad>"
UNK_TOK = "<unk>"
RAND_SEED = 6
DEVICE = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
MAX_VOCAB_SIZE_LARGE = 30_000
MAX_VOCAB_SIZE_SMALL = 10_000
BATCH_SIZE = 128

WORD_EMBEDDING_DIM = 100
CNN_PARAMS = {'n_filters': 50, 'filter_sizes': [2,3,4], 'dropout': 0.5}
RNN_HIDDEN_SIZE = 128
VDCNN_PARAMS = {'vector_size': 16, 'kernel_size': 3, 'stride': 1, 'padding': 1}