import sys
import os
import pickle

import torch

import global_variables as gv
sys.path.append(os.path.join(gv.REPO_PATH, 'src'))
import filter_concepts_extraction as fce
from classifiers import ShallowCNN


def main():
    
    nb_arg = len(sys.argv)
    if nb_arg < 2:
        print('''Insufficient number of command line arguments. Usage: 
              'python3 extract_topK_sentences.pyt {DATA_NAME}', where 
              DATA_NAME can be one of 'News_Title' or 'News_Description'. ''')
        return
    if nb_arg > 2:
        print('''Too many command line arguments. Usage: 
              'python3 extract_topK_sentences.pyt {DATA_NAME}', where 
              DATA_NAME can be one of 'News_Title' or 'News_Description'. ''')
        return 
    
    data_name = sys.argv[1]
    if data_name == 'Sentiment':
        print('''Top-K sentence extraction is only implemented for AG News 
              dataset, i.e. for data name 'News_Title' or 'News_Description'. ''')
        return
    if not data_name in ('News_Title', 'News_Description'):
        print(data_name, ''' is not a valid data name. Must be one of 
              'News_Title' or 'News_Description'. ''')
        return
    
    folder_name = data_name + "_ShallowCNN"
    DEST_PATH = os.path.join(gv.REPO_PATH, 'outputs', folder_name)
    DATA_PATH = os.path.join(gv.REPO_PATH, 'news_data')
    
    field_vocab = pickle.load(open(os.path.join(DEST_PATH, 'field_vocab.pickle'), 'rb'))
    label_vocab = pickle.load(open(os.path.join(DEST_PATH, 'label_vocab.pickle'), 'rb'))
    
    INPUT_DIM = len(field_vocab)
    OUTPUT_DIM = len(label_vocab)
    PAD_IDX = field_vocab.stoi[gv.PAD_TOK]
    
    model = ShallowCNN(INPUT_DIM, OUTPUT_DIM, PAD_IDX, gv.WORD_EMBEDDING_DIM, 
                       **gv.CNN_PARAMS)
    model.load_state_dict(torch.load(os.path.join(DEST_PATH,"model.pt"), map_location=gv.DEVICE))
    
    fce.top_k_sentences(model, data_name, DEST_PATH, DATA_PATH, gv.nlp.tokenizer, 
                        gv.DEVICE, field_vocab, K = 10, min_len = 5)


if __name__ == '__main__':
    
    main()