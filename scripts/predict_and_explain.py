import sys
import os
import pickle

import torch

import global_variables as gv
sys.path.append(os.path.join(gv.REPO_PATH, 'src'))
import explainers as X
import classifiers
import one_hot_encoders as ohe




def main():
    
    nb_arg = len(sys.argv)
    if nb_arg < 3:
        print("Insufficient number of command line arguments. Usage: \
              'python3 train_model.py {DATA_NAME} {MODEL_TYPE}', where \
              DATA_NAME can be one of 'Sentiment', 'News_Title' or \
              'News_Description', and MODEL_TYPE can be one of 'ShallowCNN', \
              'DeeperCNN', 'RNN', 'VDCNN_17' or 'VDCNN_29'.")
        return
    elif nb_arg > 3:
        print("Too many command line arguments. Usage: 'python3 \
              train_model.py {DATA_NAME} {MODEL_TYPE}', where DATA_NAME can be \
              one of 'Sentiment', 'News_Title' or 'News_Description', and \
              MODEL_TYPE can be one of 'ShallowCNN', 'DeeperCNN', 'RNN', \
              'VDCNN_17' or 'VDCNN_29'.")
        return
    
    data_name = sys.argv[1]
    model_type = sys.argv[2]
    
    if data_name == 'Sentiment':
        OUTPUT_DIM = 1
    elif data_name in ('News_Title', 'News_Description'):
        OUTPUT_DIM = 4
    else: 
        print(data_name, ''' is not a valid data name. Must be one of 
              'Sentiment', 'News_Title' or 'News_Description'. ''')
              
    folder_name = data_name + "_" + model_type
    DEST_PATH = os.path.join(gv.REPO_PATH, 'outputs', folder_name)
    # DATA_PATH = os.path.join(gv.REPO_PATH, 'news_data')
    
    field_vocab = pickle.load(open(os.path.join(DEST_PATH, 'field_vocab.pickle'), 'rb'))
    label_vocab = pickle.load(open(os.path.join(DEST_PATH, 'label_vocab.pickle'), 'rb'))
    
    INPUT_DIM = len(field_vocab)
    PAD_IDX = field_vocab.stoi[gv.PAD_TOK]
    
    if model_type in ('ShallowCNN', 'DeeperCNN'):
        
        if model_type == 'ShallowCNN':
            model = classifiers.ShallowCNN(INPUT_DIM, OUTPUT_DIM, PAD_IDX, 
                                           gv.WORD_EMBEDDING_DIM, **gv.CNN_PARAMS)
            message = "Choose an option (type and enter corresponding number): \n \
            - [0]: EXIT PROGRAM; \n\
            - [1]: make a new prediction; \n \
            - [2]: explain prediction with ngrams; \n \
            - [3]: explain prediction with filters; \n \
            - [4]: explain with words; \n \
            - [5]: explain specific ngram with words; \n \
            - [6]: explain specific ngram with filters; \n \
            - [7]: explain with word embedding features."
            
        elif model_type == 'DeeperCNN':
            model = classifiers.DeeperCNN(INPUT_DIM, OUTPUT_DIM, PAD_IDX, 
                                          gv.WORD_EMBEDDING_DIM, **gv.CNN_PARAMS)
            message = "Choose an option (type and enter corresponding number): \n  \
            - [0]: EXIT PROGRAM; \n \
            - [1]: make a new prediction; \n \
            - [2]: explain prediction with ngrams; \n \
            - [3]: explain prediction with filters; \n  \
            - [4]: explain with words; \n  \
            - [5]: explain specific ngram with words; \n  \
            - [6]: explain specific ngram with filters; \n \
            - [7]: explain with word embedding features; \n \
            - [8]: explain with groups of ngrams."

        model.load_state_dict(torch.load(os.path.join(DEST_PATH, "model.pt"), map_location=gv.DEVICE))
            
        while True:
            print("Please type in an input text to get a prediction (type EXIT to quit program): ")
            text = str(input())
            if text == 'EXIT':
                return 
                
            explanation = X.Explainer_CNN(model, text, gv.nlp.tokenizer, 
                                          field_vocab, label_vocab, 
                                          gv.DEVICE)
            explanation.show_output()
        
            print(message)
                  
            while True:
                
                option = int(input())
                if option == 0:
                    return
                elif option == 1:
                    break
                elif option == 2:
                    explanation.explain_with_ngrams(ALT = 'Business')
                elif option == 3:
                    explanation.explain_with_filters(ALT = 'Business')
                elif option == 4:
                    explanation.explain_with_words(ALT = 'Business')
                elif option == 5:
                    print("Provide length of ngram of interest: ")
                    N = int(input())
                    print("Provide start index of ngram of interest (indexing starts at 0): ")
                    INDEX = int(input())
                    explanation.explain_ngram_with_words(N, INDEX, ALT = 'Business')
                elif option == 6:
                    print("Provide length of ngram of interest: ")
                    N = int(input())
                    print("Provide start index of ngram of interest (indexing starts at 0): ")
                    INDEX = int(input())
                    explanation.explain_ngram_with_filters(N, INDEX, ALT = 'Business')   
                elif option == 7: 
                    explanation.explain_with_word_embedding_features()
                elif model_type == 'DeeperCNN' and option == 8:
                    explanation.explain_with_groups_of_ngrams(DEST_PATH)
                else:
                    print("The option you chose is invalid. ")
                        
        
    elif model_type in ('VDCNN_17','VDCNN_29'):
        
        if model_type == 'VDCNN_17':
            model = classifiers.VDCNN_17(INPUT_DIM, OUTPUT_DIM, **gv.VDCNN_PARAMS)
        else:
            model = classifiers.VDCNN_29(INPUT_DIM, OUTPUT_DIM, **gv.VDCNN_PARAMS) 
            
        model.load_state_dict(torch.load(os.path.join(DEST_PATH, "model.pt")))
        
        while True:
            print("Please type in an input text to get a prediction (type EXIT to quit program): ")
            text = str(input())
            if text == 'EXIT':
                return 
            
            
            explanation = X.Explainer_VDCNN(model, text, field_vocab, 
                                            label_vocab, gv.DEVICE)
            
            if model_type == 'VDCNN_17':
                arguments = ohe.OneHot_VDCNN((64,), 
                                             (64, explanation.model.res_blocks[1].mid["Pooled indices"]),
                                             (128,),
                                             (128, explanation.model.res_blocks[3].mid["Pooled indices"]),
                                             (256,),
                                             (256, explanation.model.res_blocks[5].mid["Pooled indices"]),
                                             (512,),
                                             (512,), **gv.VDCNN_PARAMS)
            elif model_type == 'VDCNN_29':
                arguments = ohe.OneHot_VDCNN((64,), 
                                             (64,),
                                             (64,),
                                             (64,),
                                             (64, explanation.model.res_blocks[4].mid["Pooled indices"]),
                                             (128,),
                                             (128,),
                                             (128,),
                                             (128,),
                                             (128, explanation.model.res_blocks[9].mid["Pooled indices"]),
                                             (256,),
                                             (256, explanation.model.res_blocks[11].mid["Pooled indices"]),
                                             (512,),
                                             (512,), **gv.VDCNN_PARAMS)
                
            print("Please wait, calculating relevance...")
            
            explanation.show_output()
        
            print("Choose an option (type and enter corresponding number): \n \
            - [0]: EXIT PROGRAM; \n \
            - [1]: make a new prediction; \n \
            - [2]: explain prediction with input characters; \n \
            - [3]: explain with relevance at given layer; \n  \
            - [4]: explain with relevance at given layer (decomposed into groups of characters); \n  \
            - [5]: explain with relevance at given filter; \n  \
            - [6]: explain with relevance of biases. ")
                  
            while True:
                
                option = int(input())
                if option == 0:
                    return
                elif option == 1:
                    break
                elif option == 2:
                    explanation.explain_with_chars(DEST_PATH)
                elif option == 3:
                    keys = list(explanation.net_rel.keys())
                    print("Provide the layer name. Possible values are : ", keys)
                    layer_name = str(input())
                    explanation.explain_with_layer(layer_name, arguments, DEST_PATH)
                elif option == 4:
                    keys = list(explanation.net_rel.keys())
                    print("Provide the layer name. Possible values are : ", keys)
                    layer_name = str(input())
                    explanation.explain_layer_with_chars(layer_name, arguments, DEST_PATH)
                elif option == 5:
                    keys = list(explanation.net_rel.keys())
                    print("Provide the layer name. Possible values are : ", keys)
                    layer_name = str(input())
                    print("Please provide a filter number: ")
                    filter_nb = int(input())
                    explanation.explain_with_filter(layer_name, filter_nb, arguments, DEST_PATH)
                elif option == 6:
                    keys = list(explanation.net_rel.keys())
                    print("Provide the layer name from which you wish to see the biases. Possible values are : ", keys)
                    explanation.explain_with_biases(layer_name)  
                else:
                    print("The option you chose is invalid. ")        
        
            
    elif model_type == 'RNN':
        model = classifiers.RNN(INPUT_DIM, OUTPUT_DIM, PAD_IDX, 
                                gv.WORD_EMBEDDING_DIM, gv.RNN_HIDDEN_SIZE)
        model.load_state_dict(torch.load(os.path.join(DEST_PATH, "model.pt")))

        while True:
            
            print("Please type in an input text to get a prediction (type EXIT to quit program): ")
            text = str(input())
            if text == 'EXIT':
                return 
            
            explanation = X.Explainer_RNN(model, text, gv.nlp.tokenizer, 
                                          field_vocab, label_vocab, 
                                          gv.DEVICE)
            explanation.show_output()
            explanation.explain_output(ALT = 'Sports')
        
    
    else:
        print(model_type, " is not a valid model type. Must be one of \
              'ShallowCNN', 'DeeperCNN', 'RNN', 'VDCNN_17' or 'VDCNN_29'. ")
        return



if __name__ == "__main__":
    main()