import sys
import pickle
import os

import torch
import torch.nn as nn
import torch.optim as optim

import global_variables as gv
sys.path.append(os.path.join(gv.REPO_PATH, 'src'))

import utils.preprocessing_functions as pf
import utils.train_functions as tr
import classifiers


def main():
    
    nb_arg = len(sys.argv)
    if nb_arg < 3:
        print('''Insufficient number of command line arguments. Usage: 
              'python3 train_model.py {DATA_NAME} {MODEL_TYPE}', where 
              DATA_NAME can be one of 'Sentiment', 'News_Title' or 
              'News_Description', and MODEL_TYPE can be one of 'ShallowCNN', 
              'DeeperCNN', 'RNN', 'VDCNN_17' or 'VDCNN_29'. ''')
        return
    elif nb_arg > 3:
        print('''Too many command line arguments. Usage: 'python3 
              train_model.py {DATA_NAME} {MODEL_TYPE}', where DATA_NAME can be 
              one of 'Sentiment', 'News_Title' or 'News_Description', and 
              MODEL_TYPE can be one of 'ShallowCNN', 'DeeperCNN', 'RNN', 
              'VDCNN_17' or 'VDCNN_29'. ''')
        return
    
    data_name = sys.argv[1]
    model_type = sys.argv[2]
    
    folder_name = data_name + "_" + model_type
    DEST_PATH = os.path.join(gv.REPO_PATH, 'outputs', folder_name)


    if data_name == 'Sentiment':
        criterion = nn.BCEWithLogitsLoss()
        OUTPUT_DIM = 1
        result = pf.preprocess(data_name, model_type, gv.REPO_PATH, gv.RAND_SEED, 
                           gv.MAX_VOCAB_SIZE_LARGE, gv.BATCH_SIZE, gv.DEVICE)
    elif data_name in ('News_Title', 'News_Description'):
        criterion = nn.CrossEntropyLoss()
        OUTPUT_DIM = 4
        result = pf.preprocess(data_name, model_type, gv.REPO_PATH, gv.RAND_SEED, 
                           gv.MAX_VOCAB_SIZE_SMALL, gv.BATCH_SIZE, gv.DEVICE)
    else: 
        print(data_name, ''' is not a valid data name. Must be one of 
              'Sentiment', 'News_Title' or 'News_Description'. ''')
        return
    
    TEXT, LABEL, train_it, valid_it, test_it = result
    
    with open(DEST_PATH + "/field_vocab.pickle", 'wb') as f:
        pickle.dump(TEXT.vocab, f)
        print("Field vocab saved in ", DEST_PATH)

    with open(DEST_PATH + "/label_vocab.pickle", 'wb') as f:
        pickle.dump(LABEL.vocab, f)
        print("Label vocab saved in ", DEST_PATH)
    
    INPUT_DIM = len(TEXT.vocab)
    
    if model_type in ('ShallowCNN', 'DeeperCNN', 'RNN'):
        
        PAD_IDX = TEXT.vocab.stoi[TEXT.pad_token]
        UNK_IDX = TEXT.vocab.stoi[TEXT.unk_token]
        N_EPOCHS = 1
        
        if model_type == 'ShallowCNN':
            model = classifiers.ShallowCNN(INPUT_DIM, OUTPUT_DIM, PAD_IDX, 
                                           gv.WORD_EMBEDDING_DIM, **gv.CNN_PARAMS)
        
        elif model_type == 'DeeperCNN':
            model = classifiers.DeeperCNN(INPUT_DIM, OUTPUT_DIM, PAD_IDX, 
                                          gv.WORD_EMBEDDING_DIM, **gv.CNN_PARAMS)
        else:
            model = classifiers.RNN(INPUT_DIM, OUTPUT_DIM, PAD_IDX, 
                                    gv.WORD_EMBEDDING_DIM, gv.RNN_HIDDEN_SIZE)

        model.embedding.weight.data.copy_(TEXT.vocab.vectors)
        model.embedding.weight.data[UNK_IDX] = torch.zeros(gv.WORD_EMBEDDING_DIM)
        model.embedding.weight.data[PAD_IDX] = torch.zeros(gv.WORD_EMBEDDING_DIM)
        
    
    
    elif model_type in ('VDCNN_17', 'VDCNN_29'):
        
        # N_EPOCHS = 15
        N_EPOCHS = 1

        if model_type == 'VDCNN_17':
            model = classifiers.VDCNN_17(INPUT_DIM, OUTPUT_DIM, **gv.VDCNN_PARAMS)
        else:
            model = classifiers.VDCNN_29(INPUT_DIM, OUTPUT_DIM, **gv.VDCNN_PARAMS)
    
    else:
        print(model_type, ''' is not a valid model type. Must be one of 
              'ShallowCNN', 'DeeperCNN', 'RNN', 'VDCNN_17' or 'VDCNN_29'. ''')
        return
    
    
    # Network optimizer
    optimizer = optim.Adam(model.parameters())
    model = model.to(gv.DEVICE)
    criterion = criterion.to(gv.DEVICE)
    
    # print(DEST_PATH)
    if not os.path.isdir(DEST_PATH):
        os.makedirs(DEST_PATH)
    
    print("Training and evaluating ", model_type, " with data ", data_name)
    tr.train_and_evaluate(model, optimizer, criterion, data_name, 
                          model_type, train_it, valid_it, test_it, 
                          DEST_PATH, N_EPOCHS)




if __name__ == '__main__':
    
    main()
    
  
    
  
    